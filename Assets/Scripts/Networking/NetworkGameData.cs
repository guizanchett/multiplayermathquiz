using System;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;


public class NetworkGameData : INetworkSerializable
{
    //ROUND DATA
    private int _currentRound;
    private int _currentQuestion;
    private int _questionsLeftThisRound;
    private string _questionTimeString;
    
    
    //QUESTION DATA
    private float _currentQuestionAnswer;
    private int _currentDifficulty;
    private string _currentQuestionInfixExpression;
    
    private List<float> _alternativesList;

    public NetworkGameData()
    {
        _currentRound = 1;
        _currentQuestion = 1;
        _currentDifficulty = 1;
        _questionTimeString = "00:00";
        _alternativesList = new List<float>();
    }

    public void ResetGameData()
    {
        _currentRound = 1;
        _currentQuestion = 1;
        _currentDifficulty = 1;
        _questionsLeftThisRound = ScriptableSettings.AMOUNT_OF_QUESTIONS_IN_ROUND;
        _questionTimeString = "00:00";
        _alternativesList = new List<float>();
    }
    
    public void SetNetworkQuestion(float answer, int difficulty, string infixExpression)
    {
        _currentQuestionAnswer = answer;
        _currentDifficulty = difficulty;
        _currentQuestionInfixExpression = infixExpression;
    }

    public void UpdateQuestionAndRoundData(int currentRound, int currentQuestion, int questionsLeft)
    {
        _currentRound = currentRound;
        _currentQuestion = currentQuestion;
        _questionsLeftThisRound = questionsLeft;
    }
    
    public float GetQuestionAnswer()
    {
        return _currentQuestionAnswer;
    }
    
    public void GetNetworkQuestionData(out float answer, out int difficulty, out string infixExpression)
    {
        answer = _currentQuestionAnswer;
        difficulty = _currentDifficulty;
        infixExpression = _currentQuestionInfixExpression;
    }

    public void GetQuestionAndRoundData(out int currentRound,out int currentQuestion,out int questionsLeft)
    {
        currentRound = _currentRound;
        currentQuestion = _currentQuestion;
        questionsLeft = _questionsLeftThisRound;
    }
    
    public void UpdateTimeStringData(string timeString)
    {
        _questionTimeString = timeString;
    }
    
    public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        serializer.SerializeValue(ref _currentQuestionAnswer);
        serializer.SerializeValue(ref _currentDifficulty);
        serializer.SerializeValue(ref _currentQuestionInfixExpression);
    }
}
