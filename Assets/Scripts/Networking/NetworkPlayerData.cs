using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

[Serializable]
public class NetworkPlayerData : INetworkSerializable
{
    private string _playerName;
    private float _currentAnswer;
    private string _playerID;
    private int _lastQuestionScore;
    private int _currentScore;
    private int _correctAnswerCount;
    private bool _correctLastAnswer;

    public NetworkPlayerData()
    {
        _playerName = "";
        _playerID = "";
        _currentAnswer = -9999999;
        _correctLastAnswer = false;
        _correctAnswerCount = 0;
        _currentScore = 0;
        _lastQuestionScore = 0;
    }
    
    public NetworkPlayerData(string playerName, string id)
    {
        _playerName = playerName;
        _playerID = id;
        _currentAnswer = -9999999;
        _correctLastAnswer = false;
        _correctAnswerCount = 0;
        _currentScore = 0;
        _lastQuestionScore = 0;
    }

    public void ResetPlayerData()
    {
        _currentAnswer = -9999999;
        _correctLastAnswer = false;
        _correctAnswerCount = 0;
        _currentScore = 0;
        _lastQuestionScore = 0;
    }
    
    //SETTING THE PLAYER
    public void UpdatePlayerNetworkData(string playerName, string iD)
    {
        _playerName = playerName;
        _playerID = iD;
    }
    
    //SETTING THE CURRENT ANSWER
    public void UpdatePlayerNetworkData(float answer, bool correctAnswer, int lastQuestionScore, int totalScore, int correctAnswerCount)
    {
        _currentAnswer = answer;
        _correctLastAnswer = correctAnswer;
        _lastQuestionScore = lastQuestionScore;
        _currentScore = totalScore;
        _correctAnswerCount = correctAnswerCount;
    }
    
    //SETTING ROUND DATA
    public void UpdatePlayerNetworkData(string iD, float answer, bool correctAnswer, int lastQuestionScore, int currentScore, int correctAnswerCount)
    {
        _currentAnswer = answer;
        _lastQuestionScore = lastQuestionScore;
        _currentScore = currentScore;
        _correctLastAnswer = correctAnswer;
        _correctAnswerCount = correctAnswerCount;
    }
    
    public float GetPlayerAnswer()
    {
        return _currentAnswer;
    }

    public void GetPlayerCredentials(out string playerName, out string playerID)
    {
        playerName = _playerName;
        playerID = _playerID;
    }
    
    public void GetPlayerQuestionResultInfo(out string playerID, out string playerName, out float currentAnswer, out int lastQuestionScore, out bool correctAnswer)
    {
        playerID = _playerID;
        playerName = _playerName;
        currentAnswer = _currentAnswer;
        correctAnswer = _correctLastAnswer;
        lastQuestionScore = _lastQuestionScore;
    }
    
    public void GetPlayerData(out string playerID, out string playerName, out float currentAnswer, out int lastQuestionScore, out bool correctAnswer, out int currentScore, out int correctAnswerCount)
    {
        playerID = _playerID;
        playerName = _playerName;
        currentAnswer = _currentAnswer;
        correctAnswer = _correctLastAnswer;
        correctAnswerCount = _correctAnswerCount;
        lastQuestionScore = _lastQuestionScore;
        currentScore = _currentScore;
    }
    
    public void GetPlayerRoundSummary(out string playerName, out int currentScore, out int correctAnswers)
    {
        playerName = _playerName;
        currentScore = _currentScore;
        correctAnswers = _correctAnswerCount;
    }
    
    
    public bool CheckPlayerId(string iDToCheck)
    {
        if(iDToCheck != _playerID) return false;

        return true;
    }
    
    public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        serializer.SerializeValue(ref _playerName);
        serializer.SerializeValue(ref _currentAnswer);
        serializer.SerializeValue(ref _playerID);
        serializer.SerializeValue(ref _lastQuestionScore);
        serializer.SerializeValue(ref _currentScore);
        serializer.SerializeValue(ref _correctAnswerCount);
        serializer.SerializeValue(ref _correctLastAnswer);
        
    }
}
