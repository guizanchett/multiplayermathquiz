using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Netcode;
using Unity.Services.Lobbies;
using UnityEngine;

public class NetworkServicesHandler : NetworkBehaviour
{
    [SerializeField] private ScriptableSettings settings;
    
    private NetworkGameData _networkGameData;
    [SerializeField] private List<NetworkPlayerData> _networkPlayerDataList = new List<NetworkPlayerData>();
    
    public static Action OnGameSetupStart;
    public static Action OnGameRestart;
    public static Action OnRoundStart;
    public static Action<int> OnAllPlayersSet;
    public static Action<int, int, int> OnGameDataReset;
    public static Action<int, int, int> OnQuestionAndRoundDataUpdated;
    public static Action<float, int, string> OnQuestionUpdated;
    public static Action<int, int, float, float, int, int, int, int> OnSettingsUpdated;
    public static Action<List<NetworkPlayerData>> OnQuestionResultUpdated;
    public static Action<List<NetworkPlayerData>> OnSummaryUpdated;
    public static Action<List<NetworkPlayerData>> OnFinalInfoUpdated;
    
    public override void OnNetworkSpawn()
    {
        PlayerComponent.OnPlayerAnswerFinal += SendPlayerAnswerUpdate;
        PlayerComponent.OnPlayerNetworkSetup += InformPlayerSpawned;
        RoundManager.OnTimeOver += InformQuestionResultUpdate;
        RoundManager.OnRoundAndQuestionUpdated += UpdateQuestionAndRoundData;
        RoundManager.OnRequestRestartGame += RestartGame;
        RoundManager.OnInitEndGame += PrepareFinalInfo;
        RoundUIController.OnQuestionResultsClosed += InformRoundSummaryUpdated;
        RoundUIController.OnStartCountEnded += StartGame;
        SettingsManager.OnSettingUpdate += InformSettingsUpdated;
        Question.OnQuestionDataUpdate += InformUpdateQuestion;
        
        if (!IsOwner) return;
        _networkGameData = new NetworkGameData();
    }
    
    private void StartGame()
    {
        OnGameSetupStart?.Invoke();
    }
    
    public void RestartGame()
    {
        if (!IsServer) return;

        ResetPlayerData();

        _networkGameData.ResetGameData();
        _networkGameData.GetQuestionAndRoundData(out int currentRound, out int currentQuestion, out int questionsLeft);

        OnGameDataReset?.Invoke(currentRound, currentQuestion, questionsLeft);
        BroadcastRestartGameRpc(currentRound, currentQuestion, questionsLeft);
    }

    [Rpc(SendTo.Everyone)]
    private void BroadcastRestartGameRpc(int currentRound, int currentQuestion, int questionsLeft)
    {
       OnGameRestart?.Invoke();
       BroadCastQuestionAndRoundDataUpdateRpc(currentRound, currentQuestion,questionsLeft);
       InformRoundSummaryUpdated();
       StartGame();
    }
    
    private void ResetPlayerData()
    {
        foreach (var playerData in _networkPlayerDataList)
        {
            playerData.ResetPlayerData();
            
            playerData.GetPlayerData(out string playerID, out string playerName, out float currentAnswer, out int lastQuestionScore, out bool correctAnswer, out int currentScore, out int correctAnswerCount);

            BroadcastResetPlayerDataRpc(playerID, currentAnswer, lastQuestionScore, correctAnswer, currentScore, correctAnswerCount);
        }
    }

    [Rpc(SendTo.ClientsAndHost)]
    private void BroadcastResetPlayerDataRpc(string playerID, float currentAnswer, int lastQuestionScore, bool correctAnswer, int currentScore, int correctAnswerCount)
    {
        SendPlayerAnswerUpdate(playerID, currentAnswer, correctAnswer, 0, lastQuestionScore,
            currentScore, correctAnswerCount);
    }
    
    private void InformPlayerSpawned(string playerName, string playerID)
    {
        Debug.Log($"RECEIVED PLAYER DATA {playerName}, {playerID}");
        
        SetPlayerOnServerRpc(playerName, playerID);
    }
    
    [Rpc(SendTo.Server)]
    private void SetPlayerOnServerRpc(string playerName, string playerId)
    {
        NetworkPlayerData newPlayerData = new NetworkPlayerData(playerName, playerId);
     
        CustomDebugger.DebugHighlighted($"ADDING PLAYER {playerName} TO NETWORK LIST");
        
        _networkPlayerDataList.Add(newPlayerData);

        if (!IsServer) return;

        if (NetworkManager.ConnectedClients.Count() == _networkPlayerDataList.Count)
        {
            OnAllPlayersSet?.Invoke(NetworkManager.ConnectedClients.Count);
        }
    }


    
    private void UpdateQuestionAndRoundData(int currentRound, int currentQuestion, int questionsLeft)
    {
        _networkGameData.UpdateQuestionAndRoundData(currentRound, currentQuestion, questionsLeft);
        BroadCastQuestionAndRoundDataUpdateRpc(currentRound, currentQuestion, questionsLeft);
    }

    [Rpc(SendTo.ClientsAndHost)]
    private void BroadCastQuestionAndRoundDataUpdateRpc(int currentRound, int currentQuestion, int questionsLeft)
    {
        OnQuestionAndRoundDataUpdated?.Invoke(currentRound, currentQuestion, questionsLeft);
    }
    
    private void PrepareFinalInfo()
    {
        OrderFinalInfo();
        InformRoundFinalInfoUpdated();
    }

    private void OrderFinalInfo()
    {
        if (!IsServer) return;
        for (int i = 0; i < _networkPlayerDataList.Count; i++)
        {
            _networkPlayerDataList[i].GetPlayerRoundSummary(out string playerNameI, out int playerScoreI, out int correctAnswersI);
            _networkPlayerDataList[^1].GetPlayerRoundSummary(out string playerNameLast, out int playerScoreLast,
                out int correctAnswersLast);

            if (playerScoreLast >= playerScoreI)
            {
                (_networkPlayerDataList[i], _networkPlayerDataList[^1]) = (_networkPlayerDataList[^1], _networkPlayerDataList[i]);
            }
        }
        
        _networkPlayerDataList[0].GetPlayerRoundSummary(out string playerNameN1, out int playerScoreN1, out int correctAnswersN1);
        _networkPlayerDataList[1].GetPlayerRoundSummary(out string playerNameN2, out int playerScoreN2, out int correctAnswersN2);

        if (playerScoreN2 > playerScoreN1)
        {
            (_networkPlayerDataList[0], _networkPlayerDataList[1]) = (_networkPlayerDataList[1], _networkPlayerDataList[0]);
        }
    }

    private void InformUpdateQuestion(float answer, int difficulty, string infixExpression)
    {
        UpdateQuestionDataRpc(answer, difficulty, infixExpression);
    }

    [Rpc(SendTo.Server)]
    private void UpdateQuestionDataRpc(float answer, int difficulty, string infixExpression)
    {
        _networkGameData.SetNetworkQuestion(answer, difficulty, infixExpression);

        _networkGameData.GetNetworkQuestionData(out float ans, out int dif, out string exp);

        BroadcastQuestionUpdatedRpc(ans, dif, exp);
    }

    [Rpc(SendTo.ClientsAndHost)]
    private void BroadcastQuestionUpdatedRpc(float answer, int difficulty, string infixExpression)
    {
        OnQuestionUpdated?.Invoke(answer, difficulty, infixExpression);

        if (!IsServer) return;
        BroadcastRoundStartRpc();
    }

    [Rpc(SendTo.Everyone)]
    private void BroadcastRoundStartRpc()
    {
        OnRoundStart?.Invoke();
    }

    private void SendPlayerAnswerUpdate(string iD, float answer, bool correctAnswer, int difficulty, int lastScore,
        int totalScore, int correctAnswerCount)
    {
        UpdatePlayerAnswerRpc(iD, answer, correctAnswer, difficulty, lastScore, totalScore, correctAnswerCount);
    }

    [Rpc(SendTo.Server)]
    private void UpdatePlayerAnswerRpc(string iD, float answer, bool correctAnswer, int difficulty, int lastScore,
        int totalScore, int correctAnswerCount)
    {
        foreach (var playerData in _networkPlayerDataList)
        {
            if (playerData.CheckPlayerId(iD))
            {
                playerData.UpdatePlayerNetworkData(answer, correctAnswer, lastScore, totalScore, correctAnswerCount);
                playerData.GetPlayerCredentials(out string playerName, out string playerID);
            }
        }
    }

    private void InformQuestionResultUpdate()
    {
        if (!IsOwner) return;
        OnQuestionResultUpdated?.Invoke(_networkPlayerDataList);
    }

    private void InformRoundSummaryUpdated()
    {
        if (!IsOwner) return;
        OnSummaryUpdated?.Invoke(_networkPlayerDataList);
    }

    private void InformRoundFinalInfoUpdated()
    {
        if (!IsOwner) return;
        OnFinalInfoUpdated?.Invoke(_networkPlayerDataList);
    }

    private void InformSettingsUpdated()
    {
        if(!IsServer) return;
        BroadcastNewSettingsRpc(settings.amountOfRounds, settings.amountOfQuestionsInRound,
            settings.startingRoundDuration, settings.timeIncreaseMultiplier, settings.alternativesNumber, settings.easyQuestionScore, settings.mediumQuestionScore, settings.hardQuestionScore);
    }

    [Rpc(SendTo.Everyone)]
    private void BroadcastNewSettingsRpc(int amountOfRounds, int amountOfQuestions, float startingRoundDuration, float timeIncreaseMultiplier, int alternativesNumber, int easyScore, int mediumScore, int hardScore)
    {
        OnSettingsUpdated?.Invoke(amountOfRounds, amountOfQuestions, startingRoundDuration, timeIncreaseMultiplier, alternativesNumber, easyScore, mediumScore, hardScore);
    }
    
    public override void OnNetworkDespawn()
    {
        PlayerComponent.OnPlayerAnswerFinal -= SendPlayerAnswerUpdate;
        PlayerComponent.OnPlayerNetworkSetup -= InformPlayerSpawned;
        RoundManager.OnTimeOver -= InformQuestionResultUpdate;
        RoundManager.OnRoundAndQuestionUpdated -= UpdateQuestionAndRoundData;
        RoundManager.OnRequestRestartGame -= RestartGame;
        RoundManager.OnInitEndGame -= PrepareFinalInfo;
        RoundUIController.OnQuestionResultsClosed -= InformRoundSummaryUpdated;
        RoundUIController.OnStartCountEnded -= StartGame;
        SettingsManager.OnSettingUpdate -= InformSettingsUpdated;
        Question.OnQuestionDataUpdate -= InformUpdateQuestion;
    }
}