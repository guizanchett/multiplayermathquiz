using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class LobbyPlayerInfo : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI playerNameText;
    [SerializeField] private Image playerImage;
    [SerializeField] private Image playerReadyImage;
    [SerializeField] private ScriptableSettings settings;

    public string PlayerID;
    public string PlayerName;
    public int PlayerSpriteIndex;
    
    
    public void SetPlayerLobbyInfo(string playerName, Sprite readySprite, string ID, int spriteIndex)
    {
        playerNameText.text = playerName;
        PlayerID = ID;
        PlayerName = playerName;
        playerReadyImage.sprite = readySprite;
        PlayerSpriteIndex = spriteIndex;
        playerImage.sprite = settings.playerPossibleSprites[PlayerSpriteIndex];
    }

}
