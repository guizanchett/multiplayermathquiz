using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DG.Tweening;
using Sirenix.Utilities;
using TMPro;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using Unity.Networking.Transport.Relay;
using Unity.Services.Authentication;
using Unity.Services.Core;
using Unity.Services.Lobbies;
using Unity.Services.Lobbies.Models;
using Unity.Services.Relay;
using Unity.Services.Relay.Models;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class LobbyManager : NetworkBehaviour
{
    public static LobbyManager Instance;
    
    private const string KEY_RELAY_JOIN_CODE = "RelayJoinCode";
    private const string KEY_START_GAME = "0";
    
    [SerializeField] private GameObject lobbyPlayerInfo;
    [SerializeField] private Transform lobbyPlayerInfoParent;
    [SerializeField] private Transform openingPanel;
    [SerializeField] private Transform lobbyActionsPanel;
    [SerializeField] private Transform lobbyCreationPanel;
    [SerializeField] private Transform lobbyPanel;
    [SerializeField] private Transform gameSettingsPanel;
    [SerializeField] private Transform loadingScreen;
    [SerializeField] private TMP_InputField lobbyCodeField;
    [SerializeField] private TMP_InputField playerNameInputField;
    [SerializeField] private TMP_InputField lobbyNameInputField;
    [SerializeField] private TMP_InputField playerLimitInputField;
    [SerializeField] private TextMeshProUGUI playerNameText;
    [SerializeField] private TextMeshProUGUI lobbyNameText;
    [SerializeField] private TextMeshProUGUI lobbyCodeText;
    [SerializeField] private GameObject playerNameObject;
    [SerializeField] private GameObject startGameButton; 
    [SerializeField] private List<Sprite> playerReadySprites;
    [SerializeField] private ScriptableSettings settings;
    
    private string _playerName;
    private string _isReady = "false";
    
    private int _maxPlayers;
    private int _spriteIndex;
 
    private float _awakeSignalTimer;
    private float _lobbyUpdateTimer;
    
    private bool _keepLobbyAwake;
    private bool _updateLobby;
    
    private List<LobbyPlayerInfo> _lobbyPlayerInfoList;
    
    private Player _playerData;
    private Lobby _currentLobby;

    public static Action OnProcessStartGame;
    public static Action<ulong> OnClientRequestDisconnect;

    private async void Start()
    {
        Instance = this;
        _spriteIndex = Random.Range(0, settings.playerPossibleSprites.Count);

        OnClientRequestDisconnect += DisconnectClientByID;
        
        await UnityServices.InitializeAsync();
        
        if (!AuthenticationService.Instance.IsSignedIn)
        {
            await AuthenticationService.Instance.SignInAnonymouslyAsync();
        }
        
    }

    private void Update()
    {
        HandleLobbyUpdatePoll();
        KeepLobbyAwake();
    }
    
    private async void KeepLobbyAwake()
    {
        if (_currentLobby != null && _keepLobbyAwake)
        {
            _awakeSignalTimer -= Time.deltaTime;

            if (_awakeSignalTimer <= 0f)
            {
                float awakeLimit = 15f;
                _awakeSignalTimer = awakeLimit;

               await LobbyService.Instance.SendHeartbeatPingAsync(_currentLobby.Id);
            }
        }
    }

    private async void HandleLobbyUpdatePoll()
    {
        if (!_updateLobby) return;
        
        if (_currentLobby != null)
        {
            _lobbyUpdateTimer -= Time.deltaTime;
            if (_lobbyUpdateTimer <= 0f)
            {
                float lobbyUpdateMaxTime = 1.5f;

                _lobbyUpdateTimer = lobbyUpdateMaxTime;
                Lobby lobby = await LobbyService.Instance.GetLobbyAsync(_currentLobby.Id);
                _currentLobby = lobby;
                
                ManageLobbyPlayerInfo();
                
                UpdatePlayerLobbyInfo();
            }
        }
    }
    
    public async void CreateNewLobby()
    {
        try
        {
            OpenLoadingScreen();
            SetPlayer(_playerName, _spriteIndex.ToString());
            CreateLobbyOptions lobbyOptions = new CreateLobbyOptions
            {
                IsPrivate = true,
                Player = GetPlayer(),
                Data = new Dictionary<string, DataObject>
                {
                    {KEY_START_GAME, new DataObject(DataObject.VisibilityOptions.Member,"0")}
                }
            };

            _maxPlayers = Int32.Parse(playerLimitInputField.text);
            
            Lobby lobby = await LobbyService.Instance.CreateLobbyAsync(lobbyNameInputField.text.ToUpper(), _maxPlayers, lobbyOptions);
            _currentLobby = lobby;
            
            Allocation allocation = await AllocateRelay();

            string relayJoinCode = await GetRelayCode(allocation);

            await LobbyService.Instance.UpdateLobbyAsync(_currentLobby.Id, new UpdateLobbyOptions
            {
                Data = new Dictionary<string, DataObject>
                {
                    {KEY_RELAY_JOIN_CODE, new DataObject(DataObject.VisibilityOptions.Member, relayJoinCode)}
                }
            });
            
            NetworkManager.Singleton.GetComponent<UnityTransport>().SetRelayServerData(new RelayServerData(allocation, "dtls"));
            NetworkManager.Singleton.StartHost();
            
            _keepLobbyAwake = true;
            _updateLobby = true;

            settings.SetSettings();
            
            CloseLoadingScreen();
            OpenLobbyPanel();
        }
        catch (LobbyServiceException exception)
        {
            CloseLoadingScreen();
            CustomDebugger.DebugLobbyException(exception);
        }
    }

    public async void JoinLobbyByCode()
    {
        try
        {
            OpenLoadingScreen();
            SetPlayer(_playerName, _spriteIndex.ToString());
            JoinLobbyByCodeOptions joinLobbyOptions = new JoinLobbyByCodeOptions
            {
                Player = GetPlayer()
            };
            
            _currentLobby = await Lobbies.Instance.JoinLobbyByCodeAsync(lobbyCodeField.text, joinLobbyOptions);

            string relayJoinCode = _currentLobby.Data[KEY_RELAY_JOIN_CODE].Value;
            
            JoinAllocation joinAllocation = await JoinRelayByCode(relayJoinCode);
            
            NetworkManager.Singleton.GetComponent<UnityTransport>().SetRelayServerData(new RelayServerData(joinAllocation, "dtls"));

            NetworkManager.Singleton.StartClient();
            
            CloseLoadingScreen();
            OpenLobbyPanel();
            
            _updateLobby = true;
        }
        catch (LobbyServiceException exception)
        {
            CloseLoadingScreen();
            CustomDebugger.DebugLobbyException(exception);
        }
    }
    
    private async Task<Allocation> AllocateRelay()
    {
        try
        {
            Allocation relayAllocation = await RelayService.Instance.CreateAllocationAsync(_maxPlayers - 1);

            return relayAllocation;
        }
        catch (RelayServiceException exception)
        {
            CustomDebugger.DebugRelayException(exception);
            return default;
        }
    }

    private async Task<string> GetRelayCode(Allocation relayAllocation)
    {
        try
        {
            string relayCode = await RelayService.Instance.GetJoinCodeAsync(relayAllocation.AllocationId);

            return relayCode;
        }
        catch (RelayServiceException exception)
        {
            CustomDebugger.DebugRelayException(exception);
            return default;
        }
    }

    private async Task<JoinAllocation> JoinRelayByCode(string relayCode)
    {
        try
        {
            JoinAllocation joinAllocation = await RelayService.Instance.JoinAllocationAsync(relayCode);

            return joinAllocation;
        }
        catch (RelayServiceException exception)
        {
            CustomDebugger.DebugRelayException(exception);

            return default;
        }
    }
    
    private void ManageLobbyPlayerInfo()
    {

        if (_currentLobby != null && _currentLobby.Players.Count > 0)
        {
            if (_lobbyPlayerInfoList.IsNullOrEmpty())
            {
                _lobbyPlayerInfoList = new List<LobbyPlayerInfo>();
            }
            
            if (_lobbyPlayerInfoList.Count > _currentLobby.Players.Count)
            {
                Debug.Log($"PLAYER COUNT {_currentLobby.Players.Count} LOBBY COUNT LIST {_lobbyPlayerInfoList.Count}");
                
                for (int i = _lobbyPlayerInfoList.Count-1; i >= 0; i--)
                {
                    bool remove = true;
                    
                    foreach (var player in _currentLobby.Players)
                    {
                        if (player.Data["PlayerID"].Value == _lobbyPlayerInfoList[i].PlayerID)
                        {
                            remove = false;
                        }
                    }

                    if (remove)
                    {
                        _lobbyPlayerInfoList[i].gameObject.SetActive(false);
                        _lobbyPlayerInfoList.RemoveAt(i);
                    }
                }

                return;
            }
            
            if(_currentLobby.Players.Count == _lobbyPlayerInfoList.Count) return; 
            
            if (_lobbyPlayerInfoList.Count == 0)
            {

                GameObject newLobbyPlayerInfo = Instantiate(lobbyPlayerInfo, lobbyPlayerInfoParent);
                    
                LobbyPlayerInfo playerInfoComponent = newLobbyPlayerInfo.GetComponent<LobbyPlayerInfo>();
                    
                Sprite readySprite = playerReadySprites[0];
                    
                if (_currentLobby.Players[0].Data["PlayerReady"].Value == "true")
                {
                    readySprite = playerReadySprites[1];
                }
                    
                playerInfoComponent.SetPlayerLobbyInfo(_currentLobby.Players[0].Data["PlayerName"].Value, readySprite, _currentLobby.Players[0].Data["PlayerID"].Value, Int32.Parse(_currentLobby.Players[0].Data["PlayerSpriteIndex"].Value));

                _lobbyPlayerInfoList.Add(playerInfoComponent);
            }
            else
            {
                foreach (var player in _currentLobby.Players)
                {
                    foreach (var lobbyInfo in _lobbyPlayerInfoList)
                    {
                        bool playerInList = false;

                        if (player.Data["PlayerID"].Value == lobbyInfo.PlayerID)
                        {
                            playerInList = true;
                        }

                        if (!playerInList)
                        {

                            GameObject newLobbyPlayerInfo = Instantiate(lobbyPlayerInfo, lobbyPlayerInfoParent);
                            LobbyPlayerInfo playerInfoComponent = newLobbyPlayerInfo.GetComponent<LobbyPlayerInfo>();
                    
                            Sprite readySprite = playerReadySprites[0];
                    
                            if (player.Data["PlayerReady"].Value == "true")
                            {
                                readySprite = playerReadySprites[1];
                            }
                    
                            playerInfoComponent.SetPlayerLobbyInfo(player.Data["PlayerName"].Value, readySprite, player.Data["PlayerID"].Value, Int32.Parse(player.Data["PlayerSpriteIndex"].Value));
   
                            _lobbyPlayerInfoList.Add(playerInfoComponent);

                            if (_currentLobby.Players.Count == _lobbyPlayerInfoList.Count) break; 
                        }
                    }
                }
            }
        }
    }

    public async void TogglePlayerReady()
    {
        try
        {
            if (_isReady == "true")
            {
                _isReady = "false";
            }
            else if (_isReady == "false")
            {
                _isReady = "true";
            }    
        
            CustomDebugger.DebugHighlighted($"PLAYER DATA IS {GetPlayer().Data["PlayerName"].Value}, {_isReady}, {GetPlayer().Data["PlayerSpriteIndex"].Value}, {GetPlayer().Data["PlayerID"].Value}");
            
            await LobbyService.Instance.UpdatePlayerAsync(_currentLobby.Id, AuthenticationService.Instance.PlayerId,
                new UpdatePlayerOptions
                {
                    Data = new Dictionary<string, PlayerDataObject>
                    {
                        {"PlayerName", new PlayerDataObject(PlayerDataObject.VisibilityOptions.Member, GetPlayer().Data["PlayerName"].Value) },
                        {"PlayerReady", new PlayerDataObject(PlayerDataObject.VisibilityOptions.Member, _isReady)},
                        {"PlayerSpriteIndex", new PlayerDataObject(PlayerDataObject.VisibilityOptions.Member, GetPlayer().Data["PlayerSpriteIndex"].Value)},
                        {"PlayerID", new PlayerDataObject(PlayerDataObject.VisibilityOptions.Member, GetPlayer().Data["PlayerID"].Value)}
                    }
                });
        }
        catch (LobbyServiceException exception)
        {
            CustomDebugger.DebugLobbyException(exception);
        }
        
        UpdatePlayerLobbyInfo();
    }

    private void UpdatePlayerLobbyInfo()
    {
        if (_lobbyPlayerInfoList.IsNullOrEmpty() || _currentLobby == null) return;
        
        if (AuthenticationService.Instance.PlayerId == _currentLobby.HostId && !_keepLobbyAwake)
        {
            _keepLobbyAwake = true;
        }

        bool allReady = true;
        
        foreach (var player in _currentLobby.Players)
        {
            foreach (var lobbyInfo in _lobbyPlayerInfoList)
            {
                if (player.Data["PlayerID"].Value == lobbyInfo.PlayerID)
                {
                    Sprite readySprite = playerReadySprites[0];
                
                    if (player.Data["PlayerReady"].Value == "true")
                    {
                        readySprite = playerReadySprites[1];
                    }
                    else
                    {
                        allReady = false;
                    }

                    lobbyInfo.SetPlayerLobbyInfo(player.Data["PlayerName"].Value, readySprite, player.Data["PlayerID"].Value, Int32.Parse(player.Data["PlayerSpriteIndex"].Value));

                }
            }
        }

        if (allReady && AuthenticationService.Instance.PlayerId == _currentLobby.HostId)
        {
            startGameButton.SetActive(true);
        }
    }

    public void StartGame()
    {
        Debug.Log("STARTING THE GAME");
        NetworkManager.Singleton.SceneManager.LoadScene("GameScene", LoadSceneMode.Additive);
        BroadcastGameStartRpc();
    }

    [Rpc(SendTo.ClientsAndHost)]
    private void BroadcastGameStartRpc()
    {
        if (IsServer)
        {
            BroadcastSettingsUpdateRpc(settings.amountOfRounds, settings.amountOfQuestionsInRound, settings.startingRoundDuration, settings.timeIncreaseMultiplier, settings.easyQuestionScore, settings.mediumQuestionScore, settings.hardQuestionScore, settings.alternativesNumber);
        }
        OnProcessStartGame?.Invoke();
    }
    
    [Rpc(SendTo.ClientsAndHost)]
    private void BroadcastSettingsUpdateRpc(int _amountOfRounds, int _amountOfQuestionsInRound, float _startingRoundDuration, float _timeIncreaseMultiplier, int _easyQuestionScore, int _mediumQuestionScore, int _hardQuestionScore, int _alternativesNumber)
    {
        Debug.Log($"SHOULD BE UPDATING SETTINGS {_amountOfRounds}, {_amountOfQuestionsInRound}");
        settings.SetSettings(_amountOfRounds, _amountOfQuestionsInRound, _startingRoundDuration, _timeIncreaseMultiplier, _easyQuestionScore, _mediumQuestionScore, _hardQuestionScore, _alternativesNumber);
    }
    
    public async void GetLobbiesList()
    {
        try
        {
            QueryResponse queryResponse = await Lobbies.Instance.QueryLobbiesAsync();
            
            foreach (var lobby in queryResponse.Results)
            {
                Debug.Log($"Lobby {lobby.Name} {lobby.MaxPlayers}");   
            }
        }
        catch (LobbyServiceException exception)
        {
            CustomDebugger.DebugLobbyException(exception);
        }

    }
    
    private void SetPlayer(string playerName, string spriteIndex)
    {
        
        CustomDebugger.DebugHighlighted($"SETTING PLAYER NAME TO {playerName}");
        
        _playerData = new Player
        {
            Data = new Dictionary<string, PlayerDataObject>
            {
                {"PlayerName", new PlayerDataObject(PlayerDataObject.VisibilityOptions.Member, playerName.ToUpper()) },
                {"PlayerReady", new PlayerDataObject(PlayerDataObject.VisibilityOptions.Member, _isReady)},
                {"PlayerSpriteIndex", new PlayerDataObject(PlayerDataObject.VisibilityOptions.Member, spriteIndex)},
                {"PlayerID", new PlayerDataObject(PlayerDataObject.VisibilityOptions.Member, AuthenticationService.Instance.PlayerId)}
            }
        };
    }

    public Player GetPlayer()
    {
        return _playerData;
    }
    
    public async void LeaveLobby()
    {
        _updateLobby = false;
        _keepLobbyAwake = false;
        _isReady = "false";
        
        try
        {
            if (!_lobbyPlayerInfoList.IsNullOrEmpty())
            {
                LobbyPlayerInfo infoToRemove = _lobbyPlayerInfoList[0];
                
                foreach (var playerInfo in _lobbyPlayerInfoList)
                {
                    if (playerInfo.PlayerID == AuthenticationService.Instance.PlayerId)
                    {
                        infoToRemove = playerInfo;
                    }
                }
                
                _lobbyPlayerInfoList.Remove(infoToRemove);
                infoToRemove.gameObject.SetActive(false);
            }
            
            await LobbyService.Instance.RemovePlayerAsync(_currentLobby.Id, AuthenticationService.Instance.PlayerId);

            if (IsServer)
            {
                NetworkManager.Shutdown();
            }
            else
            {
                RequestClientDisconnect(NetworkManager.Singleton.LocalClientId);
            }

            _currentLobby = null;

            lobbyPanel.DOScale(Vector3.zero, 0.25f).OnComplete(() =>
            {
                lobbyPanel.gameObject.SetActive(false);

                lobbyActionsPanel.gameObject.SetActive(true);
                lobbyActionsPanel.DOScale(Vector3.one, 0.45f).SetEase(Ease.OutBounce);
            });

        }
        catch (LobbyServiceException exception)
        {
            CustomDebugger.DebugLobbyException(exception);
        }
    }


    private void RequestClientDisconnect(ulong clientID)
    {
        BroadcastRequestClientDisconnectRpc(clientID);
    }

    [Rpc(SendTo.Server)]
    private void BroadcastRequestClientDisconnectRpc(ulong clientID)
    {
        if(!IsServer) return;
        OnClientRequestDisconnect?.Invoke(clientID);
    }

    private void DisconnectClientByID(ulong clientID)
    {
        if(!IsServer) return;
        NetworkManager.Singleton.DisconnectClient(clientID);
    }
    
    public void SetPlayerName()
    {
        _playerName = playerNameInputField.text;

        playerNameText.text = _playerName.ToUpper();

        playerNameObject.SetActive(true);
        playerNameObject.transform.DOScale(Vector3.one, 0.15f);

        openingPanel.DOScale(Vector3.zero, 0.25f).OnComplete(() =>
        {
            openingPanel.gameObject.SetActive(false);
            lobbyActionsPanel.gameObject.SetActive(true);

            lobbyActionsPanel.DOScale(Vector3.one, 0.45f).SetEase(Ease.OutBounce);
        });
    }

    public void OpenLobbyCreationPanel()
    {
        lobbyActionsPanel.DOScale(Vector3.zero, 0.25f).OnComplete(() =>
        {
            lobbyActionsPanel.gameObject.SetActive(false);
            
            lobbyCreationPanel.gameObject.SetActive(true);
            lobbyCreationPanel.DOScale(Vector3.one, 0.45f).SetEase(Ease.OutBounce);
        });
    }
    
    public void CloseLobbyCreationPanel()
    {
        lobbyNameInputField.text = null;
        playerLimitInputField.text = null;
        
        lobbyCreationPanel.DOScale(Vector3.zero, 0.25f).OnComplete(() =>
        {
            lobbyCreationPanel.gameObject.SetActive(false);
            
            lobbyActionsPanel.gameObject.SetActive(true);
            lobbyActionsPanel.DOScale(Vector3.one, 0.45f).SetEase(Ease.OutBounce);
        });
    }

    public void OpenGameSettingsPanel()
    {
        lobbyCreationPanel.DOScale(Vector3.zero, 0.25f).OnComplete(() =>
        {
            lobbyCreationPanel.gameObject.SetActive(false);
            
            gameSettingsPanel.gameObject.SetActive(true);
            gameSettingsPanel.DOScale(Vector3.one, 0.45f).SetEase(Ease.OutBounce);
        });
    }

    public void CloseGameSettingsPanel()
    {
        gameSettingsPanel.DOScale(Vector3.zero, 0.25f).OnComplete(() =>
        {
            gameSettingsPanel.gameObject.SetActive(false);
            
            lobbyCreationPanel.gameObject.SetActive(true);
            lobbyCreationPanel.DOScale(Vector3.one, 0.45f).SetEase(Ease.OutBounce);
        });
    }
    
    private void OpenLobbyPanel()
    {
        lobbyNameInputField.text = null;
        playerLimitInputField.text = null;
        
        lobbyCreationPanel.DOScale(Vector3.zero, 0.25f).OnComplete(() =>
        {
            lobbyActionsPanel.gameObject.SetActive(false);
            lobbyPanel.gameObject.SetActive(true);

            lobbyNameText.text = _currentLobby.Name;
            lobbyCodeText.text = $"LOBBY CODE: <color=black><b>{_currentLobby.LobbyCode}</b></color>";
            
            lobbyPanel.DOScale(Vector3.one, 0.45f).SetEase(Ease.OutBounce);
        });
    }

    private void OpenLoadingScreen()
    {
        loadingScreen.gameObject.SetActive(true);
        loadingScreen.DOScale(Vector3.one, 0.25f);
    }
    
    private void CloseLoadingScreen()
    {
        loadingScreen.DOScale(Vector3.one, 0.25f).OnComplete(() =>
        {
            loadingScreen.gameObject.SetActive(false);
        });
    }

    public override void OnNetworkDespawn()
    {
        _keepLobbyAwake = false;
        _updateLobby = false;
        OnClientRequestDisconnect -= DisconnectClientByID;
    }
}
