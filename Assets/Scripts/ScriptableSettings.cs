using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu()]
public class ScriptableSettings : ScriptableObject
{
    [Title("QUIZ SETTINGS")] 
    public int amountOfRounds;
    public int amountOfQuestionsInRound;
    public float startingRoundDuration;
    public float timeIncreaseMultiplier;
    
    [Space(15)]
    [Title("QUESTIONS SETTINGS")]
    public int alternativesNumber;
    public int easyQuestionScore;
    public int mediumQuestionScore;
    public int hardQuestionScore;
    
    [Space(15)]
    [Title("MATH SETTINGS")]
    public Vector2 easyRange;
    public Vector2 mediumRange;
    public Vector2 hardRange;

    [Space(15)]
    [Title("DEBUG SETTINGS")] 
    public bool disableGeneralDebugLogs;
    public bool disableMathDebugLogs;
    public bool disableQuestionLogs;
    public bool disableNetworkServicesLogs;

    [Space(15)]
    [Title("LOBBY INFO SETTINGS")]
    public List<Sprite> playerPossibleSprites;

    [Space(15)] 
    [Title("AUDIO SETTINGS")]
    public AudioClip mainMenuTrack;
    public AudioClip gameTrack;
    public AudioClip selectQuestionSfx;
    public AudioClip confirmQuestionSfx;
    
    #region STATIC PROPERTIES
    
    public static int NUM_ALTERNATIVES = 0;
    public static int AMOUNT_OF_ROUNDS = 0;
    public static int AMOUNT_OF_QUESTIONS_IN_ROUND = 0;
    public static float STARTING_ROUND_DURATION = 0;
    private static int EASY_SCORE;
    private static int MEDIUM_SCORE;
    private static int HARD_SCORE;
    
    public static float TIME_INCREASE_MULTIPLIER = 0f;

    public static bool DISABLE_DEBUG_LOGS = false;
    public static bool DISABLE_NETWORK_SERVICES_DEBUG_LOGS = false;
    public static bool DISABLE_MATH_DEBUG_LOGS = false;
    public static bool DISABLE_QUESTION_LOGS = false;

    public static Vector2 EASY_RANGE = new Vector2(0, 10);
    public static Vector2 MEDIUM_RANGE = new Vector2(-20, 20);
    public static Vector2 HARD_RANGE = new Vector2(-100, 100);

    #endregion
    
    public void SetSettings()
    {
        SetDebugSettings(disableGeneralDebugLogs, disableQuestionLogs, disableMathDebugLogs, disableNetworkServicesLogs);
        SetDifficultyRanges(easyRange, mediumRange, hardRange);
        SetQuizSettings(amountOfRounds, amountOfQuestionsInRound, startingRoundDuration, timeIncreaseMultiplier);
        SetScoreSettings(easyQuestionScore, mediumQuestionScore, hardQuestionScore);
        SetQuestionSettings(alternativesNumber);
    }

    public void SetSettings(int _amountOfRounds, int _amountOfQuestionsInRound, float _startingRoundDuration, float _timeIncreaseMultiplier, int _easyQuestionScore, int _mediumQuestionScore, int _hardQuestionScore, int _alternativesNumber)
    {
        amountOfRounds = _amountOfRounds;
        amountOfQuestionsInRound = _amountOfQuestionsInRound;
        startingRoundDuration = _startingRoundDuration;
        timeIncreaseMultiplier = _timeIncreaseMultiplier;
        easyQuestionScore = _easyQuestionScore;
        mediumQuestionScore = _mediumQuestionScore;
        hardQuestionScore = _hardQuestionScore;
        alternativesNumber = _alternativesNumber;
        
        SetDebugSettings(disableGeneralDebugLogs, disableQuestionLogs, disableMathDebugLogs, disableNetworkServicesLogs);
        SetDifficultyRanges(easyRange, mediumRange, hardRange);
        SetQuizSettings(amountOfRounds, amountOfQuestionsInRound, startingRoundDuration, timeIncreaseMultiplier);
        SetScoreSettings(easyQuestionScore, mediumQuestionScore, hardQuestionScore);
        SetQuestionSettings(alternativesNumber);
    }

    
    public static int GetScore(int questionDifficulty)
    {
        switch (questionDifficulty)
        {
            case 0:
                return EASY_SCORE;
                
            case 1:
                return MEDIUM_SCORE;
            
            case 2:
                return HARD_SCORE;
                
        }

        return -1;
    }
    
    private void SetDebugSettings(bool logsState, bool questionLogsState, bool mathLogsState, bool netServicesLogState)
    {
        DISABLE_DEBUG_LOGS = logsState;
        DISABLE_MATH_DEBUG_LOGS = mathLogsState;
        DISABLE_QUESTION_LOGS = questionLogsState;
        DISABLE_NETWORK_SERVICES_DEBUG_LOGS = netServicesLogState;
    }

    private void SetDifficultyRanges(Vector2 easy, Vector2 medium, Vector2 hard)
    {
        EASY_RANGE = easy;
        MEDIUM_RANGE = medium;
        HARD_RANGE = hard;
    }

    private static void SetScoreSettings(int easy, int medium, int hard)
    {
        EASY_SCORE = easy;
        MEDIUM_SCORE = medium;
        HARD_SCORE = hard;
    }
    
    private static void SetQuestionSettings(int alternativesNum)
    {
        NUM_ALTERNATIVES = alternativesNum;
    }

    private static void SetQuizSettings(int rounds, int questionInRounds, float roundDur, float timeDecrMult)
    {
        AMOUNT_OF_ROUNDS = rounds;
        AMOUNT_OF_QUESTIONS_IN_ROUND = questionInRounds;
        STARTING_ROUND_DURATION = roundDur;
        TIME_INCREASE_MULTIPLIER = timeDecrMult;
    }


}