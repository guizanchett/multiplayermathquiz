using System.Collections.Generic;
using System.Data;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(menuName = "New Question SO", fileName = "QuestionSO", order = 0)]
public class ScriptableQuestion : ScriptableObject
{
    public bool usePreSetExpression;
    public int Dificulty;
    public string QuestionExpression;
}
