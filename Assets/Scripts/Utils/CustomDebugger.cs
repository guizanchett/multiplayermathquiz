using Unity.Services.Lobbies;
using Unity.Services.Relay;
using UnityEngine;

public static class CustomDebugger
{
    public static void DebugMath(string stringToPrint)
    {
        if(ScriptableSettings.DISABLE_MATH_DEBUG_LOGS || ScriptableSettings.DISABLE_DEBUG_LOGS) return;
        
        Debug.Log($"<color=lightblue><<size=11>MATH DEBUG: </size></color> {stringToPrint}");
    }
    
    public static void DebugMathHighlight(string stringToPrint)
    {
        if(ScriptableSettings.DISABLE_MATH_DEBUG_LOGS || ScriptableSettings.DISABLE_DEBUG_LOGS) return;
        
        Debug.Log($"<color=cyan><b><size=13>MATH DEBUG: </size></b></color> {stringToPrint}");
    }

    public static void DebugQuestion(string stringToPrint)
    {
        if (ScriptableSettings.DISABLE_QUESTION_LOGS || ScriptableSettings.DISABLE_DEBUG_LOGS) return;
        
        Debug.Log($"<color=lightgreen><b><size=11>QUESTION DEBUG: </size></b></color> {stringToPrint}");
    }
    
    public static void DebugButtonAnswerHighlight(string stringToPrint)
    {
        if (ScriptableSettings.DISABLE_QUESTION_LOGS || ScriptableSettings.DISABLE_DEBUG_LOGS) return;
        
        Debug.Log($"<color=green><b><size=13>QUESTION DEBUG: </size></b></color> {stringToPrint}");
    }
    
    public static void DebugNetworkServices(string stringToPrint)
    {
        if (ScriptableSettings.DISABLE_DEBUG_LOGS || ScriptableSettings.DISABLE_NETWORK_SERVICES_DEBUG_LOGS) return;
        
        Debug.Log($"<color=blue><b><size=13>DEBUG NETWORK SERVICES: </size></b></color> {stringToPrint}");
    }
    
    public static void DebugLobbyException(LobbyServiceException stringToPrint)
    {
        if (ScriptableSettings.DISABLE_DEBUG_LOGS) return;
        
        Debug.Log($"<color=red><b><size=14>LOBBY EXCEPTION: </size></b></color> {stringToPrint}");
    }
    
    public static void DebugRelayException(RelayServiceException stringToPrint)
    {
        if (ScriptableSettings.DISABLE_DEBUG_LOGS) return;
        
        Debug.Log($"<color=red><b><size=14>RELAY EXCEPTION: </size></b></color> {stringToPrint}");
    }
    
    public static void DebugHighlighted(string stringToPrint)
    {
        if (ScriptableSettings.DISABLE_DEBUG_LOGS) return;
        
        Debug.Log($"<color=white><b><size=14>DEBUG: </size></b></color> {stringToPrint}");
    }
}