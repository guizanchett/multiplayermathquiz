using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Reloader : MonoBehaviour
{
    private void Awake()
    {
        StartCoroutine(ReloadMainMenu());
    }

    private IEnumerator ReloadMainMenu()
    {
        yield return new WaitForSeconds(2);

        SceneManager.LoadScene("GameLobby");
    }
}
