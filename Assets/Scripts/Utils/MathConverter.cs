using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Sirenix.Utilities;
using Unity.Netcode;
using UnityEngine;
using Random = UnityEngine.Random;

public static class MathConverter 
{
    private static Queue<string> outputQueue;
    private static Stack<string> operatorStack;
    private static Stack<string> resultNumStack;
    private static Stack<string> randomizedNumStack;

    private static string infixExpression;
    
    public static Queue<string> ConvertIntoPostfix(string expressionToConvert, bool usesCustomExpression, int questionDifficulty, Question questionComponent)
    {
        outputQueue = new Queue<string>();
        operatorStack = new Stack<string>();
        
        CustomDebugger.DebugMath($"EXPRESSION TO CONVERT IS {expressionToConvert}. WITH A LENGHT OF {expressionToConvert.Length}");

        if (!usesCustomExpression)
        {
            infixExpression = RandomizeNumbers(expressionToConvert, questionDifficulty);
        }
        else
        {
            infixExpression = expressionToConvert;
        }
        
        questionComponent.SetInfixExpression(infixExpression);
        CustomDebugger.DebugMath($"------------------------------ > INFIX EXPRESSION IS {infixExpression} <------------------------------");
        
        for (int i = 0; i < infixExpression.Length; i++)
        {
            string token = infixExpression[i].ToString();
            
            if (!token.IsNullOrWhitespace()) 
            {
                if (Regex.IsMatch(token, @"^[a-zA-Z0-9]+$") && !IsOperator(token))
                {
                    string fullNumber = "";

                    for (int j = i; j < infixExpression.Length; j++)
                    {
                        if (infixExpression[j].ToString().IsNullOrWhitespace()) 
                        {
                            break;
                        }

                        fullNumber += infixExpression[j].ToString();
                        
                        i = j;
                    }
                    
                    outputQueue.Enqueue(fullNumber);
                }
                else if (IsOperator(token))
                {
                    if (IsNumber(infixExpression[i + 1].ToString()) && token == "-")
                    {
                        string number = "";
                        
                        for (int j = i; j < infixExpression[j]; j++)
                        {
                            
                            number += infixExpression[j];
      
                            if (j+1 < infixExpression.Length && infixExpression[j + 1].ToString().IsNullOrWhitespace())
                            {
                                break;
                            }

                            i = j+1;

                            if (j == infixExpression.Length-1)
                            {
                                break;
                            }
                        }
                        
                        CustomDebugger.DebugMath($"ENQUEUING NEGATIVE NUMBER {number}");
                        
                        outputQueue.Enqueue(number);
                    }
                    else
                    {
                        while (operatorStack.Count != 0 && GetPrecedence(operatorStack.Peek()) >= GetPrecedence(token))
                        {
                            CustomDebugger.DebugMath($"STACK NOT EMPTY. {GetPrecedence(operatorStack.Peek())} HAS A HIGHER OR EQUAL PRECEDENCE THAN {GetPrecedence(token)}. QUEUEING {operatorStack.Peek()}");
                            outputQueue.Enqueue(operatorStack.Pop());
                        }
                    
                        operatorStack.Push(token);
                    }

                }
                else if (token == "(")
                {
                    operatorStack.Push(token);
                }
                else if (token == ")")
                {
                   
                    while (operatorStack.Peek() != "(")
                    {
                        outputQueue.Enqueue(operatorStack.Pop());
                    }
                    
                    operatorStack.Pop();
                }
                CustomDebugger.DebugMathHighlight($"-------------------------> OUTPUT QUEUE [{GetQueueString(outputQueue)}]\n-------------------------> OPERATOR STACK [{GetStackString(operatorStack)}]");
            }
        }
        while (operatorStack.Count != 0)
        {
            outputQueue.Enqueue(operatorStack.Pop());
        }

        CustomDebugger.DebugMathHighlight($"FINAL QUEUE IS [{GetQueueString(outputQueue)}]");
        
        return outputQueue;
    }

    public static float SolvePostfixExpression(Queue<string> postfixExpression)
    {
        CustomDebugger.DebugMathHighlight($"--------------> SOLVING EXPRESSION. POST FIX EXPRESSION IS: [{GetQueueString(postfixExpression)}] <--------------");

        var queueToList = postfixExpression.ToList();
        
        resultNumStack = new Stack<string>();
        
        for (int i = 0; i < queueToList.Count(); i++)
        {
            string lastOnStack = "";

                string currentCharacter = queueToList[i];
                
                if (IsNumber(queueToList[i]))
                {
                    resultNumStack.Push(currentCharacter);
                }

                if (IsOperator(queueToList[i]))
                {

                    float newNum = 0;

                    if (i+1 < queueToList.Count-1 && queueToList[i] == "-" && IsNumber(queueToList[i + 1]))
                    {
                        string numberString = queueToList[i] + queueToList[i + 1];

                        resultNumStack.Push(numberString);
                        
                        i++;
                    }
                    else
                    {
                        lastOnStack = resultNumStack.Pop();
                        
                        newNum = ResolveOperation(float.Parse(resultNumStack.Pop()), float.Parse(lastOnStack),
                            currentCharacter);
                        
                        resultNumStack.Push(newNum.ToString());
                    }
                    
                }
            
            CustomDebugger.DebugMathHighlight($"-----------> RESULT STACK CURRENTLY IS: [{GetStackString(resultNumStack)}]");
            CustomDebugger.DebugMathHighlight($"-----------> REMAINING QUEUE CURRENTLY IS: [{GetListString(queueToList)}]");
        }
        
        CustomDebugger.DebugMathHighlight($"------------------- RESULT IS {resultNumStack.Peek()}  -------------------");
        return float.Parse(resultNumStack.Pop());
    }

    
    private static string RandomizeNumbers(string stringToRandomize, int difficulty)
    {
        string randomNumbersString = "";

        int newRandomNumber = 0;
        
        for (int i = 0; i < stringToRandomize.Length; i++)
        {
            string currentChar = stringToRandomize[i].ToString();
            string numberToAdd = "";
            if (!currentChar.IsNullOrWhitespace() && !IsOperator(currentChar))
            {
                if (!IsNumber(currentChar) && currentChar != "(" && currentChar != ")")
                {
                    switch (difficulty)
                    {
                        case 0:
                            newRandomNumber = Random.Range((int)ScriptableSettings.EASY_RANGE.x,
                                (int)ScriptableSettings.EASY_RANGE.y);

                            newRandomNumber = Mathf.RoundToInt(newRandomNumber);
                            numberToAdd = newRandomNumber.ToString();
                            break;
                        case 1:
                            newRandomNumber = Random.Range((int)ScriptableSettings.MEDIUM_RANGE.x,
                                (int)ScriptableSettings.MEDIUM_RANGE.y);
                            newRandomNumber = Mathf.RoundToInt(newRandomNumber);
                            numberToAdd = newRandomNumber.ToString();
                            break;
                        case 2:
                            newRandomNumber = Random.Range((int)ScriptableSettings.HARD_RANGE.x,
                                (int)ScriptableSettings.HARD_RANGE.y);
                            newRandomNumber = Mathf.RoundToInt(newRandomNumber);
                            numberToAdd = newRandomNumber.ToString();
                            break;
                    }
                    
                    randomNumbersString += numberToAdd;
                }
                else if (currentChar == "(" || currentChar == ")")
                {
                    randomNumbersString += currentChar;
                }
            }
            else
            {
                randomNumbersString += currentChar;
            }
        }
        
        CustomDebugger.DebugMathHighlight($"RANDOMIZED STRING IS {randomNumbersString}");
        return randomNumbersString;
    }

    private static bool IsNumber(string token)
    {
        if (token.IsNullOrWhitespace()) return false;
        
        return double.TryParse(token, out _);
    }
    
    private static bool IsLetter(string token)
    {
        return double.TryParse(token, out _);
    }
    
    private static bool IsOperator(string token)
    {
        if (token.IsNullOrWhitespace()) return false;
        
        return token == "/" || token == "*" || token == "+" || token == "-";
    }

    private static float ResolveOperation(float num1, float num2, string operatorChar)
    {
        if (operatorChar == "+")
        {
            CustomDebugger.DebugMath($"SOLVING {num1} + {num2} = {num1 + num2}");
            return num1 + num2;
        }
        
        if (operatorChar == "-")
        {
            CustomDebugger.DebugMath($"SOLVING {num1} - {num2} = {num1 - num2}");
            return num1 - num2;
        }
        
        if (operatorChar == "*")
        {
            CustomDebugger.DebugMath($"SOLVING {num1} * {num2} = {num1 * num2}");
            return num1 * num2;
        }
        
        if (operatorChar == "/")
        {
            if (num1 != 0 && num2 != 0)
            {
                CustomDebugger.DebugMath($"SOLVING {num1} / {num2} = {num1 / num2}");
                return num1 / num2;
            }

            return 0;
        }

        return -1;
    }

    private static string GetNegativeNumber(string operatorString, string number)
    {
        return operatorString + number;
    }
    
    private static string GetQueueString(Queue<string> queueToConvert)
    {
        string newString = "";

        foreach (var character in queueToConvert)
        {
            if (outputQueue.Count == 0)
            {
                newString += character;
            }
            else
            {
                newString += character + " ";
            }
            
        }
        
        return newString;
    }

    private static string GetListString(List<string> listToConvert)
    {
        string newString = "";

        foreach (var character in listToConvert)
        {
            newString += character + " ";
        }

        return newString;
    }
    
    private static string GetStackString(Stack<string> stackToConvert)
    {
        string newString = "";

        foreach (var character in stackToConvert)
        {
            if (outputQueue.Count == 0)
            {
                newString += character;
            }
            else
            {
                newString += character + " ";
            }
            
        }

        newString += "";

        return newString;
    }

    private static int GetPrecedence(string charToEvaluate)
    {
        if (charToEvaluate is "+" or "-") return 1;
        if (charToEvaluate is "*" or "/") return 2;

        return -1;
    }
}