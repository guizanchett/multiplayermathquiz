using System;
using System.Collections;
using Sirenix.Utilities;
using Unity.Netcode;
using Unity.Services.Lobbies.Models;
using UnityEngine;

public class PlayerComponent : NetworkBehaviour
{
   [SerializeField] private GameObject playerRoundResult;
   [SerializeField] private GameObject playerSummary;
   [SerializeField] private GameObject playerFinalInfo;
   
   private PlayerQuestionResult _questionResult;
   private PlayerRoundSummary _roundSummary;
   private PlayerFinalInfo _finalInfo;
   
   private string _playerName = "";
   
   private float _currentAnswer;

   private int _lastQuestionScore;
   private int _currentScore;
   private int _correctAnswerCount;

   private bool _correctAnswer;

   private Player _playerData;
   
   public string PlayerID;

   public static Action<string, string> OnPlayerNetworkSetup;
   public static Action<string, float, bool,int, int, int, int> OnPlayerAnswerFinal;

   public override void OnNetworkSpawn()
   {
      LobbyManager.OnProcessStartGame += StartPlayerNetworkSetup;
      Question.OnAnswerConfirmed += InformBroadCastAnswer;
      NetworkServicesHandler.OnGameRestart += ResetPlayerData;
      
      SetPlayerComponentOnSpawn();
   }

   private void SetPlayerComponentOnSpawn()
   {
      if(!IsOwner) return;
      _playerData = LobbyManager.Instance.GetPlayer();
      
      _playerName = _playerData.Data["PlayerName"].Value;

      PlayerID = _playerData.Data["PlayerID"].Value;
   }

   private void StartPlayerNetworkSetup()
   {
      if(!IsOwner) return;
      BroadcastPlayerNetworkSetupRpc(_playerName, PlayerID);
   }

   [Rpc(SendTo.Server)]
   private void BroadcastPlayerNetworkSetupRpc(string playerName, string id)
   {
      OnPlayerNetworkSetup?.Invoke(playerName, id);
   }
   
   private void InformBroadCastAnswer(float answer, bool isCorrect, int difficulty)
   {
      if(!IsOwner) return;
      
      _currentAnswer = answer;
      _correctAnswer = isCorrect;

      if (_correctAnswer)
      {
         _lastQuestionScore = ScriptableSettings.GetScore(difficulty);
         _currentScore += _lastQuestionScore;
      }
      else
      {
         _lastQuestionScore = 0;
      }

      if (isCorrect)
      {
         _correctAnswerCount++;
      }
      
      CustomDebugger.DebugHighlighted($"BROADCASTING ANSWER FROM {_playerName}. Answer {_currentAnswer}");
      
      BroadCastPlayerAnswerRpc(PlayerID, _currentAnswer, _correctAnswer, difficulty, _currentScore, _correctAnswerCount,
         _lastQuestionScore);
   }
   
   [Rpc(SendTo.Server)]
   private void BroadCastPlayerAnswerRpc(string playerID, float answer, bool isCorrect, int difficulty, int currentScore, int correctAnswerCount, int lastQuestionScore)
   {
      CustomDebugger.DebugHighlighted($"RECEIVING ANSWER FROM {playerID}, {answer}");
      OnPlayerAnswerFinal?.Invoke(playerID, answer, isCorrect, difficulty, lastQuestionScore, currentScore, correctAnswerCount);
   }
   
   private void ResetPlayerData()
   {
      _currentScore = 0;
      _correctAnswerCount = 0;
      _lastQuestionScore = 0;
      _currentAnswer = -99999999;
   }
   
   public override void OnNetworkDespawn()
   {
      LobbyManager.OnProcessStartGame -= StartPlayerNetworkSetup;
      Question.OnAnswerConfirmed -= InformBroadCastAnswer;
      NetworkServicesHandler.OnGameRestart -= ResetPlayerData;
   }
}
