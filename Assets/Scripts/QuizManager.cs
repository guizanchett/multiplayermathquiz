using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using Unity.Netcode;
using Unity.Services.Authentication;
using Unity.Services.Lobbies;
using Unity.Services.Relay;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class QuizManager : NetworkBehaviour
{
    [Title("QUESTION SETTINGS")]
    [SerializeField] private GameObject questionGameObject;
    [SerializeField] private List<ScriptableQuestion> possibleQuestionList;

    private Question _questionComponent;

    private List<ScriptableQuestion> _easyQuestionsList;
    private List<ScriptableQuestion> _mediumQuestionsList;
    private List<ScriptableQuestion> _hardQuestionsList;

    private ScriptableQuestion _currentQuestionSO;

    public int currentDifficulty;

    public override void OnNetworkSpawn()
    {
        _questionComponent = questionGameObject.GetComponent<Question>();

        NetworkServicesHandler.OnGameSetupStart += QuizSetup;
        RoundManager.OnRequestNewQuestion += GenerateNewQuestion;
        RoundManager.OnNewRoundStart += StartNewRound;
        RoundManager.OnInitEndGame += ResetDifficulty;

        currentDifficulty = 0;
        
        SortQuestions(possibleQuestionList);
    }
    
    public void QuizSetup()
    {
        if(!IsServer) return;
        GenerateNewQuestion();
    }

    private void ResetDifficulty()
    {
        currentDifficulty = 0;
    }
    
    private void GenerateNewQuestion()
    {
        if(!IsServer) return;
        
        switch (currentDifficulty)
        {
            case 0:
                _currentQuestionSO = GetQuestionFromCurrentDifficulty(_easyQuestionsList);

                break;
            case 1:
                _currentQuestionSO = GetQuestionFromCurrentDifficulty(_mediumQuestionsList);

                break;
            case 2:
                _currentQuestionSO = GetQuestionFromCurrentDifficulty(_hardQuestionsList);
                break;
        }
        
        _questionComponent.SetQuestion(_currentQuestionSO.QuestionExpression, _currentQuestionSO.Dificulty, _currentQuestionSO.usePreSetExpression);
    }
    
    private void NextDifficulty()
    {
        currentDifficulty++;

        if (currentDifficulty > 2)
        {
            currentDifficulty = 0;
        }
    }

    private void StartNewRound()
    {
        NextDifficulty();
        GenerateNewQuestion();
    }
    
    private void SortQuestions(List<ScriptableQuestion> questionsList)
    {
        _easyQuestionsList = new List<ScriptableQuestion>();
        _mediumQuestionsList = new List<ScriptableQuestion>();
        _hardQuestionsList = new List<ScriptableQuestion>();

        foreach (var question in questionsList)
        {
            switch (question.Dificulty)
            {
                case 0:
                    _easyQuestionsList.Add(question);
                    break;
                case 1:
                    _mediumQuestionsList.Add(question);
                    break;
                case 2:
                    _hardQuestionsList.Add(question);
                    break;
            }
        }
    }

    public void ReturnToMainMenu()
    {
        BroadcastReturnToMenuRpc();
        
        Destroy(SoundManager.Instance.gameObject);
        
        AuthenticationService.Instance.SignOut(true);
        
        NetworkManager.SceneManager.UnloadScene(SceneManager.GetSceneByName("GameScene"));
        NetworkManager.SceneManager.UnloadScene(SceneManager.GetSceneByName("GameLobby"));
        NetworkManager.Shutdown();
        
        if (NetworkManager.Singleton != null)
        {
            Destroy(NetworkManager.Singleton.gameObject);
        }
    }

    [Rpc(SendTo.Everyone)]
    private void BroadcastReturnToMenuRpc()
    {
        DOTween.KillAll();
        SceneManager.LoadScene("Reload");
    }
    
    
    private ScriptableQuestion GetQuestionFromCurrentDifficulty(List<ScriptableQuestion> questionsList)
    {
        int index = Random.Range(0, questionsList.Count);

        return questionsList[index];
    }

    public override void OnNetworkDespawn()
    {
        NetworkServicesHandler.OnGameSetupStart -= QuizSetup;
        RoundManager.OnRequestNewQuestion -= GenerateNewQuestion;
        RoundManager.OnNewRoundStart -= StartNewRound;
        RoundManager.OnInitEndGame -= ResetDifficulty;
    }
}
