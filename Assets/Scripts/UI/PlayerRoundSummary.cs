using TMPro;
using Unity.Netcode;
using UnityEngine;


public class PlayerRoundSummary : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI playerNameText;
    [SerializeField] private TextMeshProUGUI correctAnswersText;
    [SerializeField] private TextMeshProUGUI totalPointsText;

    private string _playerName;
    private int _totalScore;
    private int _correctAnswerCount;
    
    
    public void InformSummaryUpdated(string playerName, int totalScore, int correctAnswerCount)
    {
        UpdateSummaryRpc(playerName, totalScore, correctAnswerCount);
    }

    [Rpc(SendTo.Everyone)]
    private void UpdateSummaryRpc(string playerName, int totalScore, int correctAnswerCount)
    {
        _playerName = playerName;
        _totalScore = totalScore;
        _correctAnswerCount = correctAnswerCount;
        
        playerNameText.text = _playerName;
        correctAnswersText.text = _correctAnswerCount.ToString();
        totalPointsText.text = _totalScore.ToString();
    }
}
