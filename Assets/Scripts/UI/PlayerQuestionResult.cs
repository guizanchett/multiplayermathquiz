using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class PlayerQuestionResult : NetworkBehaviour
{
    [SerializeField] private Image answerStatusImage;
    [SerializeField] private Sprite correctAnswerSprite;
    [SerializeField] private Sprite incorrectAnswerSprite;
    [SerializeField] private TextMeshProUGUI playerNameText;
    [SerializeField] private TextMeshProUGUI playerPointsThisRoundText;
    

    public void InformQuestionResultUpdated(bool isCorrectAnswer, string playerName, int pointsThisQuestion)
    {
        UpdateQuestionResultRpc(isCorrectAnswer, playerName, pointsThisQuestion);
    }
    
    [Rpc(SendTo.Everyone)]
    private void UpdateQuestionResultRpc(bool isCorrectAnswer, string playerName, int pointsThisQuestion)
    {
        answerStatusImage.sprite = isCorrectAnswer ? correctAnswerSprite : incorrectAnswerSprite;

        playerNameText.text = playerName;

        playerPointsThisRoundText.text = pointsThisQuestion > 0 ? $"Points: +{pointsThisQuestion}" : $"Points: {pointsThisQuestion}";
    }
}
