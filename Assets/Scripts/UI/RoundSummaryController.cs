using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.Utilities;
using Unity.Netcode;
using UnityEngine;

public class RoundSummaryController : NetworkBehaviour
{
    [SerializeField] private List<GameObject> roundSummaryObjects;

    private List<PlayerRoundSummary> _playerRoundSummaries;
    
    public override void OnNetworkSpawn()
    {
        NetworkServicesHandler.OnAllPlayersSet += GetRoundSummaryReferences;
        NetworkServicesHandler.OnSummaryUpdated += InformRoundSummaryUpdated;
    }

    private void GetRoundSummaryReferences(int amount)
    {
        if (_playerRoundSummaries.IsNullOrEmpty())
        {
            _playerRoundSummaries = new List<PlayerRoundSummary>();
            
            CustomDebugger.DebugHighlighted("GENERATING PLAYER ROUND SUMMARY");
            
            for (int i = 0; i < amount; i++)
            {
                PlayerRoundSummary newRoundSummary = roundSummaryObjects[i].GetComponent<PlayerRoundSummary>();
                
                _playerRoundSummaries.Add(newRoundSummary);
            }

            foreach (var roundSummary in roundSummaryObjects)
            {
                roundSummary.SetActive(false);
            }
        }
    }
    
    private void InformRoundSummaryUpdated(List<NetworkPlayerData> playerData)
    {
        for (int i = 0; i < playerData.Count; i++)
        {
            playerData[i].GetPlayerRoundSummary(out string playerName, out int totalScore, out int correctAnswerCount);
            BroadCastUpdateSummaryRpc(i, playerName, totalScore, correctAnswerCount);
        }
        
        ShowRoundSummaryRpc(playerData.Count);
    }

    [Rpc(SendTo.ClientsAndHost)]
    private void BroadCastUpdateSummaryRpc(int index, string playerName, int totalScore, int correctAnswerCount)
    {
        if (_playerRoundSummaries.IsNullOrEmpty())
        {
            roundSummaryObjects[index].GetComponent<PlayerRoundSummary>().InformSummaryUpdated(playerName, totalScore, correctAnswerCount);
        }
        else
        {
            _playerRoundSummaries[index].InformSummaryUpdated(playerName, totalScore, correctAnswerCount);
        }
        
    }
    
    [Rpc(SendTo.ClientsAndHost)]
    private void ShowRoundSummaryRpc(int resultsToShow)
    {
        foreach (var roundSummaryObject in roundSummaryObjects)
        {
            roundSummaryObject.SetActive(false);
        }
        
        for (int i = 0; i < resultsToShow; i++)
        {
            roundSummaryObjects[i].SetActive(true);
            roundSummaryObjects[i].transform.DOScale(Vector3.one, 0.2f);
        }
    }

    public override void OnNetworkDespawn()
    {
        NetworkServicesHandler.OnAllPlayersSet -= GetRoundSummaryReferences;
        NetworkServicesHandler.OnSummaryUpdated -= InformRoundSummaryUpdated;
    }
}
