using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollTexture : MonoBehaviour
{
   [SerializeField] private Vector2 scrollSpeed;
   
   private Image _image;


   private void Awake()
   {
      _image = GetComponent<Image>();
      _image.material = new Material(_image.material);
   }

   private void Update()
   {
      _image.material.mainTextureOffset += scrollSpeed * Time.deltaTime;
   }
}
