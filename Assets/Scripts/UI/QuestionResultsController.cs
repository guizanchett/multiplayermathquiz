using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.Utilities;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Serialization;

public class QuestionResultsController : NetworkBehaviour
{
    [SerializeField] private List<GameObject> questionResultObjects;

    private List<PlayerQuestionResult> _playerQuestionResultsList;
    
    public override void OnNetworkSpawn()
    {
        NetworkServicesHandler.OnAllPlayersSet += GetQuestionResultReferences;
        NetworkServicesHandler.OnQuestionResultUpdated += InformQuestionsUpdated;
    }
    
    private void GetQuestionResultReferences(int amount)
    {
        if (_playerQuestionResultsList.IsNullOrEmpty())
        {
            _playerQuestionResultsList = new List<PlayerQuestionResult>();
            
            CustomDebugger.DebugHighlighted("GENERATING PLAYER QUESTION RESULTS");

            
            for (int i = 0; i < amount; i++)
            {
                PlayerQuestionResult newQuestionResult = questionResultObjects[i].GetComponent<PlayerQuestionResult>();
                
                _playerQuestionResultsList.Add(newQuestionResult);
            }

            foreach (var questionResult in questionResultObjects)
            {
                questionResult.SetActive(false);
            }
        }
    }

    private void InformQuestionsUpdated(List<NetworkPlayerData> playerData)
    {
        for (int i = 0; i < playerData.Count; i++)
        {
            playerData[i].GetPlayerQuestionResultInfo(out string playerID, out string playerName, out float currentAnswer, out int lastQuestionScore, out bool correctAnswer);
            _playerQuestionResultsList[i].InformQuestionResultUpdated(correctAnswer, playerName, lastQuestionScore);
        }
        
        ShowQuestionResultsRpc(playerData.Count);
    }
    
    [Rpc(SendTo.ClientsAndHost)]
    private void ShowQuestionResultsRpc(int resultsToShow)
    {
        foreach (var questionResultObject in questionResultObjects)
        {
            questionResultObject.SetActive(false);
        }
        
        for (int i = 0; i < resultsToShow; i++)
        {
            questionResultObjects[i].SetActive(true);
            questionResultObjects[i].transform.DOScale(Vector3.one, 0.2f);
        }
    }


    public override void OnNetworkDespawn()
    {
        NetworkServicesHandler.OnAllPlayersSet -= GetQuestionResultReferences;
        NetworkServicesHandler.OnQuestionResultUpdated -= InformQuestionsUpdated;
    }
    
}