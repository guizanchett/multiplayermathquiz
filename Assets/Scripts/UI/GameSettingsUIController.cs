using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

public class GameSettingsUIController : MonoBehaviour
{
   [SerializeField] private ScriptableSettings scriptableSettings;

   [Space(15)] [Title("GAME SETTINGS")]
   [SerializeField] private TMP_InputField amountOfRoundsInputField;
   [SerializeField] private TMP_InputField easyScoreInputField;
   [SerializeField] private TMP_InputField mediumScoreInputField;
   [SerializeField] private TMP_InputField hardScoreInputField;
   
   [Space(15)]
   [Title("ROUND SETTINGS")]
   [SerializeField] private TMP_InputField amountOfQuestionsInputField;
   [SerializeField] private TMP_InputField startingRoundDurationInputField;
   [SerializeField] private TMP_InputField timeMultiplierInputField;


   private void Awake()
   {
      amountOfRoundsInputField.text = scriptableSettings.amountOfRounds.ToString();
      easyScoreInputField.text = scriptableSettings.easyQuestionScore.ToString();
      mediumScoreInputField.text = scriptableSettings.mediumQuestionScore.ToString();
      hardScoreInputField.text = scriptableSettings.hardQuestionScore.ToString();

      amountOfQuestionsInputField.text = scriptableSettings.amountOfQuestionsInRound.ToString();
      startingRoundDurationInputField.text = scriptableSettings.startingRoundDuration.ToString();
      timeMultiplierInputField.text = scriptableSettings.timeIncreaseMultiplier.ToString();
   }

   public void SetNewValues()
   {
      scriptableSettings.amountOfRounds = Int32.Parse(amountOfRoundsInputField.text);
      scriptableSettings.easyQuestionScore = Int32.Parse(easyScoreInputField.text);
      scriptableSettings.mediumQuestionScore = Int32.Parse(mediumScoreInputField.text);
      scriptableSettings.hardQuestionScore = Int32.Parse(hardScoreInputField.text);

      scriptableSettings.amountOfQuestionsInRound = Int32.Parse(amountOfQuestionsInputField.text);
      scriptableSettings.startingRoundDuration = float.Parse(startingRoundDurationInputField.text);
      scriptableSettings.timeIncreaseMultiplier = float.Parse(timeMultiplierInputField.text);

      scriptableSettings.SetSettings();
   }
}
