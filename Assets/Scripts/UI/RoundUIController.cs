using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Serialization;

public class RoundUIController : NetworkBehaviour
{
    [Title("INTRO/OUTRO PROPERTIES")]
    [SerializeField] private float bannerDuration;
    [SerializeField] private float animationDuration;
    [SerializeField] private Ease showEasingType;
    [SerializeField] private Ease hideEasingType;
    [SerializeField] private Transform introBannerTransform;
    [SerializeField] private Transform endBannerTransform;
    [SerializeField] private Transform gameStartCountdownScreen;
    [SerializeField] private int countDownTime;
    [SerializeField] private TextMeshProUGUI startCountText;
    [SerializeField] private TextMeshProUGUI roundTitle;
    [SerializeField] private TextMeshProUGUI roundAnswer;
    [SerializeField] private TextMeshProUGUI questionTitle;
    
    [Space(15)]
    [Title("ROUND UI PROPERTIES")]
    [SerializeField] private TextMeshProUGUI durationText;
    [SerializeField] private TextMeshProUGUI roundText;
    
    [Space(15)]
    [Title("ROUND SUMMARY PROPERTIES")]
    [SerializeField] private float roundResultsDuration;
    [SerializeField] private GameObject roundResultsObject;

    [Space(15)]
    [Title("GAME SUMMARY")] 
    [SerializeField] private float gameSummaryDuration;
    [SerializeField] private GameObject gameSummaryObject;

    [Space(15)] [Title("FINAL INFO UI")]
    [SerializeField] private Transform finalInfoPanel;
    [SerializeField] private GameObject restartGameButton;

    public static Action OnIntroBannerClosed;
    public static Action OnEndBannerClosed;
    public static Action OnQuestionResultsClosed;
    public static Action OnHideSummary;
    public static Action OnStartCountEnded;
    public static Action<string> OnStartCountUpdated;

    private bool _gameStartCountDown;

    private float _startGameCountTimeElapsed;
    
    public override void OnNetworkSpawn()
    {
        RoundManager.OnRoundTimeUpdated += UpdateTimerUI;
        NetworkServicesHandler.OnQuestionUpdated += UpdateQuestionAnswer;
        NetworkServicesHandler.OnGameRestart += HideFinalInfoPanel;
        NetworkServicesHandler.OnAllPlayersSet += StartCount;
        RoundManager.OnShowSummary += ShowRoundSummary;
        RoundManager.OnTimeOver += ShowQuestionResults;
        RoundManager.OnTimeOver += ShowRoundEndBanner;
        RoundManager.OnInitEndGame += ShowFinalInfoPanel;
        OnStartCountUpdated += UpdateStartCount;
    }

    private void Update()
    {
        GameStartCountdown();
    }

    private void StartCount(int num)
    {
        if (IsServer)
        {
            _startGameCountTimeElapsed = countDownTime;
            _gameStartCountDown = true;
        }
       
        BroadcastCountStartedRpc();
    }

    [Rpc(SendTo.Everyone)]
    private void BroadcastCountStartedRpc()
    {
        gameStartCountdownScreen.gameObject.SetActive(true);
        gameStartCountdownScreen.DOScale(Vector3.one, 0.3f);
    }
    
    private void GameStartCountdown() 
    {
        if (!_gameStartCountDown) return;
        
        _startGameCountTimeElapsed -= Time.deltaTime;

        TimeSpan time = TimeSpan.FromSeconds(_startGameCountTimeElapsed);

        string timeString = $"{time.Seconds:00}";
        
        BroadcastStartCountUpdatedRpc(timeString);

        if (_startGameCountTimeElapsed <= 0)
        {
            _gameStartCountDown = false;
            BroadcastStartCountUpdatedRpc("00");
            BroadcastCloseStartCountRpc();
            InformStartCountOverRpc();
        }
    }

    [Rpc(SendTo.Everyone)]
    private void BroadcastStartCountUpdatedRpc(string secondsString)
    {
        OnStartCountUpdated?.Invoke(secondsString);
    }

    [Rpc(SendTo.Server)]
    private void InformStartCountOverRpc()
    {
        OnStartCountEnded?.Invoke();
    }

    [Rpc(SendTo.Everyone)]
    private void BroadcastCloseStartCountRpc()
    {
        gameStartCountdownScreen.DOScale(Vector3.zero, 0.2f).OnComplete(() =>
        {
            gameStartCountdownScreen.gameObject.SetActive(false);
        });
    }
    
    private void UpdateStartCount(string seconds)
    {
        startCountText.text = $"GAME STARTING IN:\n{seconds}";
    }
    
    public void ShowRoundIntroBanner(int currentRound, int currentQuestion)
    {
        roundTitle.text = $"Round {currentRound}";
        questionTitle.text = $"Question {currentQuestion}";
        introBannerTransform.DOScale(Vector3.one, animationDuration).SetEase(showEasingType).OnComplete(() =>
        {
            StartCoroutine(IntroBannerCountdown());
        });
    }

    private void HideRoundIntroBanner()
    {
        introBannerTransform.DOScale(Vector3.zero, animationDuration).SetEase(hideEasingType).OnComplete(() =>
        {
            roundText.text = roundTitle.text + "\n" + questionTitle.text;
            SetRoundUIActiveState(true);

            if (!IsServer) return;
            InformIntroBannerClosedClientRpc();
        });
    }

    [ClientRpc]
    private void InformIntroBannerClosedClientRpc()
    {
        OnIntroBannerClosed?.Invoke();
    }

    private void UpdateQuestionAnswer(float answer, int difficulty, string infixExpression)
    {
        if (Math.Abs(answer % 1) <= (Double.Epsilon * 100))
        {
            roundAnswer.text = $"CORRECT ANSWER: {answer.ToString()}";
           
        }
        else
        {
            roundAnswer.text = $"CORRECT ANSWER: {answer:F1}";
        }
        
    }
    
    private void ShowRoundEndBanner()
    {
        SetRoundUIActiveState(false);
        
        endBannerTransform.DOScale(Vector3.one, animationDuration).SetEase(showEasingType).OnComplete(() =>
        {
            StartCoroutine(EndBannerCountdown());
        });
    }

    private void HideRoundEndBanner()
    {
        endBannerTransform.DOScale(Vector3.zero, animationDuration).SetEase(hideEasingType).OnComplete(() =>
        {
            OnEndBannerClosed?.Invoke();
        });
    }
    
    private IEnumerator IntroBannerCountdown()
    {
        yield return new WaitForSeconds(bannerDuration);
        
        HideRoundIntroBanner();
    }

    private IEnumerator EndBannerCountdown()
    {
        yield return new WaitForSeconds(bannerDuration);
        
        HideRoundEndBanner();
    }
    
    private void UpdateTimerUI(string time)
    {
        durationText.text = $"Time Left:\n{time}";
    }

    private void SetRoundUIActiveState(bool newState)
    {
        roundText.gameObject.SetActive(newState);
        durationText.gameObject.SetActive(newState);
    }
    
    private void ShowQuestionResults()
    {
        roundResultsObject.SetActive(true);
        roundResultsObject.transform.DOScale(Vector3.one, animationDuration).SetEase(showEasingType).OnComplete(() =>
        {
            StartCoroutine(QuestionResultsCountdown());
        });
    }

    private void HideQuestionResults()
    {
        roundResultsObject.transform.DOScale(Vector3.zero, animationDuration).SetEase(hideEasingType).OnComplete(() =>
        {
            roundResultsObject.SetActive(false);
            BroadcastQuestionResultsClosedRpc();
        });
    }
    
    private void BroadcastQuestionResultsClosedRpc()
    {
        OnQuestionResultsClosed?.Invoke();
    }
    
    private IEnumerator QuestionResultsCountdown()
    {
        yield return new WaitForSeconds(roundResultsDuration);
        
        HideQuestionResults();
    }

    private void ShowRoundSummary()
    {
        gameSummaryObject.SetActive(true);
        gameSummaryObject.transform.DOScale(Vector3.one, animationDuration).SetEase(showEasingType).OnComplete(() =>
        {
            StartCoroutine(RoundSummaryCountdown());
        });
    }
    
    private void HideRoundSummary()
    {
        gameSummaryObject.transform.DOScale(Vector3.zero, animationDuration).SetEase(hideEasingType).OnComplete(() =>
        {
            OnHideSummary?.Invoke();
            gameSummaryObject.SetActive(false);
        });
    }

    private void ShowFinalInfoPanel()
    {
        if (IsServer)
        {
            restartGameButton.SetActive(true);
        } 
        
        finalInfoPanel.gameObject.SetActive(true);
        finalInfoPanel.DOScale(Vector3.one, animationDuration).SetEase(showEasingType);
    }

    private void HideFinalInfoPanel()
    {
        finalInfoPanel.DOScale(Vector3.zero, animationDuration).SetEase(hideEasingType).OnComplete(() =>
        {
            finalInfoPanel.gameObject.SetActive(false);
        });
    }
    
    private IEnumerator RoundSummaryCountdown()
    {
        yield return new WaitForSeconds(gameSummaryDuration);
        
        HideRoundSummary();
    }


    public override void OnNetworkDespawn()
    {
        RoundManager.OnRoundTimeUpdated -= UpdateTimerUI;
        NetworkServicesHandler.OnQuestionUpdated -= UpdateQuestionAnswer;
        NetworkServicesHandler.OnGameRestart -= HideFinalInfoPanel;
        NetworkServicesHandler.OnAllPlayersSet -= StartCount;
        RoundManager.OnShowSummary -= ShowRoundSummary;
        RoundManager.OnTimeOver -= ShowQuestionResults;
        RoundManager.OnTimeOver -= ShowRoundEndBanner;
        RoundManager.OnInitEndGame -= ShowFinalInfoPanel;
        OnStartCountUpdated -= UpdateStartCount;
    }
}