using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.Utilities;
using Unity.Netcode;
using UnityEngine;

public class PlayerFinalInfoController : NetworkBehaviour
{
    [SerializeField] private List<GameObject> finalInfoObjects;

    private List<PlayerFinalInfo> _playerFinalInfos;

    public override void OnNetworkSpawn()
    {
        NetworkServicesHandler.OnAllPlayersSet += GetFinalResultReferences;
        NetworkServicesHandler.OnFinalInfoUpdated += InformFinalInfoUpdated;
    }

    private void GetFinalResultReferences(int amount)
    {
        if (_playerFinalInfos.IsNullOrEmpty())
        {
            _playerFinalInfos = new List<PlayerFinalInfo>();
            
            for (int i = 0; i < amount; i++)
            {
                PlayerFinalInfo newFinalInfo = finalInfoObjects[i].GetComponent<PlayerFinalInfo>();
                
                _playerFinalInfos.Add(newFinalInfo);
            }

            foreach (var roundSummary in finalInfoObjects)
            {
                roundSummary.SetActive(false);
            }
        }
    }
    
    private void InformFinalInfoUpdated(List<NetworkPlayerData> playerData)
    {
        for (int i = 0; i < playerData.Count; i++)
        {
            playerData[i].GetPlayerRoundSummary(out string playerName, out int totalScore, out int correctAnswerCount);
            BroadCastUpdateFinalInfoRpc(i, playerName, totalScore);
        }
        
        ShowFinalInfoRpc(playerData.Count);
    }
    
    [Rpc(SendTo.ClientsAndHost)]
    private void BroadCastUpdateFinalInfoRpc(int index, string playerName, int totalScore)
    {
        if (_playerFinalInfos.IsNullOrEmpty())
        {
            finalInfoObjects[index].GetComponent<PlayerFinalInfo>().InformFinalInfoUpdated(playerName, totalScore, index);
        }
        else
        {
            _playerFinalInfos[index].InformFinalInfoUpdated(playerName, totalScore, index);
        }
        
    }
    
    [Rpc(SendTo.ClientsAndHost)]
    private void ShowFinalInfoRpc(int resultsToShow)
    {
        foreach (var finalInfo in finalInfoObjects)
        {
            finalInfo.SetActive(false);
        }
        
        for (int i = 0; i < resultsToShow; i++)
        {
            finalInfoObjects[i].SetActive(true);
            finalInfoObjects[i].transform.DOScale(Vector3.one, 0.2f);
        }
    }

    public override void OnNetworkDespawn()
    {
        NetworkServicesHandler.OnAllPlayersSet -= GetFinalResultReferences;
        NetworkServicesHandler.OnFinalInfoUpdated -= InformFinalInfoUpdated;
    }
}
