using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class PlayerFinalInfo : MonoBehaviour
{
   [SerializeField] private Image trophyImage;
   [SerializeField] private List<Sprite> trophiesList;
   [SerializeField] private TextMeshProUGUI playerNameText;
   [SerializeField] private TextMeshProUGUI totalPointsText;
   
   private string _playerName;
   private int _totalScore;
   private int _correctAnswerCount;
   
   public void InformFinalInfoUpdated(string playerName, int totalScore, int position)
   {
       UpdateFinalInfoRpc(playerName, totalScore,  position);
   }

   [Rpc(SendTo.Everyone)]
   private void UpdateFinalInfoRpc(string playerName, int totalScore, int position)
   {
       _playerName = playerName;
       _totalScore = totalScore;

       trophyImage.sprite = trophiesList[position];
       playerNameText.text = _playerName;
       totalPointsText.text = _totalScore.ToString();
   }
}
