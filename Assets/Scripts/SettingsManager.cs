using System;
using Unity.Netcode;
using UnityEngine;

public class SettingsManager : NetworkBehaviour
{
   [SerializeField] private ScriptableSettings scriptableSettings;

   public static Action OnSettingUpdate;

   public override void OnNetworkSpawn()
   {
      NetworkServicesHandler.OnSettingsUpdated += UpdateSettings;
      
   }
   

   public void SetSettings()
   {
      if (!IsServer) return;
      
      scriptableSettings.SetSettings();
      OnSettingUpdate?.Invoke();
   }

   private void UpdateSettings(int amountOfRounds, int amountOfQuestions, float startingRoundDuration, float timeIncreaseMultiplier, int alternativesNumber, int easyScore, int mediumScore, int hardScore)
   {
      scriptableSettings.amountOfRounds = amountOfRounds;
      scriptableSettings.amountOfQuestionsInRound = amountOfQuestions;
      scriptableSettings.startingRoundDuration = startingRoundDuration;
      scriptableSettings.timeIncreaseMultiplier = timeIncreaseMultiplier;
      scriptableSettings.alternativesNumber = alternativesNumber;
      scriptableSettings.easyQuestionScore = easyScore;
      scriptableSettings.mediumQuestionScore = mediumScore;
      scriptableSettings.hardQuestionScore = hardScore;

      if (IsServer) return;
      SetSettings();
   }

   public override void OnNetworkDespawn()
   {
      NetworkServicesHandler.OnSettingsUpdated -= UpdateSettings;
   }
}
