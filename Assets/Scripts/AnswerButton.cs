
using System;
using DG.Tweening;
using TMPro;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class AnswerButton : MonoBehaviour
{
   [SerializeField] private GameObject selectionObject;
   [SerializeField] private Button buttonComponent;
   [SerializeField] private TextMeshProUGUI buttonTextComponent;
   
   private Question _question;

   public float Answer { get; private set; }

   public void Awake()
   {
      buttonComponent = GetComponent<Button>();
      buttonTextComponent = GetComponentInChildren<TextMeshProUGUI>();
      _question = GetComponentInParent<Question>();
      Question.OnAnswerSelected += DisableSelectionObject;
   }

   public void SetAnswerButtonValue(float newValue)
   {
      buttonComponent.onClick.AddListener(SelectAnswer);
      Answer = newValue;
      
      if (Math.Abs(Answer % 1) <= (Double.Epsilon * 100))
      {
         buttonTextComponent.text = Answer.ToString();
      }
      else
      {
         buttonTextComponent.text = Answer.ToString("F1");
      }
      
      buttonComponent.interactable = true;
      selectionObject.SetActive(false); 
   }

   public void SetButtonConfirmed()
   {
      selectionObject.SetActive(false);
      buttonComponent.interactable = false;
   }
   
   private void SelectAnswer()
   {
      _question.SelectAnswer(Answer, this);
      selectionObject.SetActive(true);
   }

   private void DisableSelectionObject()
   {
      selectionObject.SetActive(false); 
   }

   public void OnDestroy()
   {
      buttonComponent.onClick.RemoveListener(SelectAnswer);
      Question.OnAnswerSelected -= DisableSelectionObject;
   }
}

