using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class RoundManager : NetworkBehaviour
{
    [SerializeField] private RoundUIController roundUIController;

    private float roundDuration;
    private int amountOfQuestions;

    private int _currentRound = 1;
    private int _currentQuestion = 1;
    private float _timeElapsed;
    
    private bool _countDown;

    public static Action<int, int, int> OnRoundAndQuestionUpdated;
    public static Action<string> OnRoundTimeUpdated; //SENDS THE CURRENT TIME IN THE STOPWATCH
    public static Action OnTimeOver; //FIRED WHEN THE TIME FOR THE CURRENT QUESTION IS OVER
    public static Action OnRequestNewQuestion; //FIRED WHEN THE RESULTS CLOSE. STARTS A NEW QUESTION IF THERE ARE QUESTIONS LEFT IN THIS ROUND
    public static Action OnShowSummary; //FIRED WHEN THE SUMMARY IS DISPLAYED
    public static Action OnNewRoundStart;
    public static Action OnRequestRestartGame;
    public static Action OnInitEndGame;
    
    public override void OnNetworkSpawn()
    {
        NetworkServicesHandler.OnRoundStart += IntroRound;
        NetworkServicesHandler.OnQuestionAndRoundDataUpdated += UpdateQuestionAndRoundData;
        NetworkServicesHandler.OnGameDataReset += ResetQuestionAndRoundData;
        RoundUIController.OnEndBannerClosed += QuestionEndControl;
        RoundUIController.OnQuestionResultsClosed += GetNextQuestion;
        RoundUIController.OnHideSummary += RoundEndControl;
        RoundUIController.OnIntroBannerClosed += SetRoundSettings;
    }
    
    private void Update()
    {
        if (!IsServer) return;
        RoundTimeControl();
    }

    public void RequestGameRestart()
    {
        if(!IsServer) return;
        ResetQuestionAndRoundData(1, 1, ScriptableSettings.AMOUNT_OF_QUESTIONS_IN_ROUND);
        BroadcastRequestGameRestartRpc();
    }

    [Rpc(SendTo.ClientsAndHost)]
    private void BroadcastRequestGameRestartRpc()
    {
        OnRequestRestartGame?.Invoke();
    }
    
    private void IntroRound()
    {
        roundUIController.ShowRoundIntroBanner(_currentRound, _currentQuestion);
    }
    
    private void RoundTimeControl() //ON A SERVER EXCLUSIVE SCENARIO THIS WOULD PROBABLY NEED TO BE MOVED. 
    {
        if (!_countDown) return;
        
        _timeElapsed -= Time.deltaTime;

        TimeSpan time = TimeSpan.FromSeconds(_timeElapsed);

        string timeString = $"{time.Minutes} : {time.Seconds}";
        
        UpdateServerTimeString(timeString);

        if (_timeElapsed <= 0)
        {
            _timeElapsed = 0;
            _countDown = false;
            UpdateServerTimeString("00:00");
            InformTimeOverClientRpc();
        }
    }

    private void UpdateServerTimeString(string timeString)
    {
        if(!IsServer) return;
        BroadCastTimeStringUpdatedRpc(timeString);
    }

    [Rpc(SendTo.ClientsAndHost)]
    private void BroadCastTimeStringUpdatedRpc(string timeString)
    {
        OnRoundTimeUpdated.Invoke(timeString);
    }

    
    [Rpc(SendTo.ClientsAndHost)]
    private void InformTimeOverClientRpc()
    {
        OnTimeOver?.Invoke();
    }
    
    private void SetRoundSettings()
    {
        if (!IsServer) return;
        
        if (_currentRound == 1 && _currentQuestion == 1)
        {
            roundDuration = ScriptableSettings.STARTING_ROUND_DURATION;
            amountOfQuestions = ScriptableSettings.AMOUNT_OF_QUESTIONS_IN_ROUND;
        }
        else if(_currentRound > 1 && _currentQuestion == 1)
        {
            float newRoundTime = Mathf.RoundToInt(roundDuration * ScriptableSettings.TIME_INCREASE_MULTIPLIER);
            
            roundDuration = newRoundTime;
        }
        
        _countDown = true;
        _timeElapsed = roundDuration;
        
        OnRoundAndQuestionUpdated.Invoke(_currentRound, _currentQuestion, amountOfQuestions);
    }


    private void ResetQuestionAndRoundData(int currentRound, int currentQuestion, int questionsLeft)
    {
        if (!IsServer) return;
            
        _currentRound = currentRound;
        _currentQuestion = currentQuestion;
        amountOfQuestions = questionsLeft;
        
        OnRoundAndQuestionUpdated.Invoke(_currentRound, _currentQuestion, amountOfQuestions);
    }
    
    private void UpdateQuestionAndRoundData(int currentRound, int currentQuestion, int questionsLeft)
    {
        if (IsServer) return;

        _currentRound = currentRound;
        _currentQuestion = currentQuestion;
        amountOfQuestions = questionsLeft;
    }
    
    private void GetNextQuestion()
    {
        if (amountOfQuestions <= 0)
        {
            ShowRoundSummary();
        }
        else
        {
            OnRequestNewQuestion?.Invoke();
        }
    }
    
    private void QuestionEndControl()
    {
        if (!IsServer) return;
        amountOfQuestions--;
        
        if (amountOfQuestions > 0)
        {
            _currentQuestion++;
        }
        else
        {
            _currentRound++;
        }
        
        OnRoundAndQuestionUpdated.Invoke(_currentRound, _currentQuestion, amountOfQuestions);
    }

    private void ShowRoundSummary()
    {
        amountOfQuestions = ScriptableSettings.AMOUNT_OF_QUESTIONS_IN_ROUND;
        _currentQuestion = 1;
        OnShowSummary?.Invoke(); 
    }
    
    private void RoundEndControl()
    {
        
        if (_currentRound > ScriptableSettings.AMOUNT_OF_ROUNDS)
        {
            OnInitEndGame?.Invoke();
        }
        else
        {
            OnNewRoundStart?.Invoke();
        }
    }

    public override void OnNetworkDespawn()
    {
        NetworkServicesHandler.OnRoundStart -= IntroRound;
        NetworkServicesHandler.OnQuestionAndRoundDataUpdated -= UpdateQuestionAndRoundData;
        NetworkServicesHandler.OnGameDataReset -= ResetQuestionAndRoundData;
        RoundUIController.OnEndBannerClosed -= QuestionEndControl;
        RoundUIController.OnQuestionResultsClosed -= GetNextQuestion;
        RoundUIController.OnHideSummary -= RoundEndControl;
        RoundUIController.OnIntroBannerClosed -= SetRoundSettings;
    }
}
