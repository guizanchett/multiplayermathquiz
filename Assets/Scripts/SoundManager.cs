using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Serialization;

public class SoundManager : MonoBehaviour
{
    private static SoundManager _instance;
        
    public static SoundManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<SoundManager>();

                if (_instance == null)
                {
                    GameObject newInstance = new GameObject("SOUND MANAGER");
                    newInstance.AddComponent<SoundManager>();
                }
            }

            return _instance;
        }
    }
    
    [SerializeField] private ScriptableSettings settings;
    [SerializeField] private AudioSource audioSource;


    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
                
        DontDestroyOnLoad(this);
        
        Question.OnAnswerSelected += PlaySelectQuestionSfx;
        Question.OnAnswerConfirmed += PlayConfirmQuestionSfx;
        LobbyManager.OnProcessStartGame += PlayGameTrack;
    }

    private void PlayGameTrack()
    {
        CheckForAudioSource();
        
        audioSource.Stop();
        audioSource.clip = settings.gameTrack;
        audioSource.Play();
    }

    private void PlayMenuTrack()
    {
        CheckForAudioSource();
        
        audioSource.Stop();
        audioSource.clip = settings.mainMenuTrack;
        audioSource.Play();
    }
    
    private void PlaySelectQuestionSfx()
    {
        CheckForAudioSource();
        
        audioSource.PlayOneShot(settings.selectQuestionSfx, 2);
    }
    
    private void PlayConfirmQuestionSfx(float arg1, bool arg2, int arg3)
    {
        CheckForAudioSource();
        
        audioSource.PlayOneShot(settings.confirmQuestionSfx, 2);
    }

    private void CheckForAudioSource()
    {
        if (audioSource == null)
        {
            audioSource = gameObject.AddComponent<AudioSource>();
        }
    }
    
    private void OnDisable()
    {
        Question.OnAnswerSelected -= PlaySelectQuestionSfx;
        Question.OnAnswerConfirmed -= PlayConfirmQuestionSfx;
        LobbyManager.OnProcessStartGame -= PlayGameTrack;
    }
}
