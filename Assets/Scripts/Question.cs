using System;
using System.Collections.Generic;
using DG.Tweening;
using DG.Tweening.Core;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using TMPro;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class Question : NetworkBehaviour
{

   [SerializeField] private NetworkServicesHandler networkServicesHandler;   
   
   [Title("QUESTION GENERAL PROPERTIES")]
   [SerializeField] private GameObject answerButtonPrefab;
   [SerializeField] private Transform buttonsLayoutGroup;

   [Space(15)] [Title("QUESTION UI")]
   [SerializeField] private Transform questionCanvas;
   [SerializeField] private Transform questionHeader;
   [SerializeField] private Transform answerCanvas;
   [SerializeField] private TextMeshProUGUI answerText;
   [SerializeField] private float questionAnimationDuration;
   [SerializeField] private Ease questionEase;
   [SerializeField] private TextMeshProUGUI questionTextComponent;
   [SerializeField] private GameObject confirmButton;
   
   private float _answer;

   private int _difficulty;
   
   private string _infixExpression;

   private float _selectedAnswer;

   private bool _correctAnswer;
   
   private AnswerButton _selectedButton;
   
   private List<float> _alternativesList;

   private List<AnswerButton> _answerButtonList;
   
   private Tweener _moveButtonTween;

   public float Answer => _answer;
   public int Difficulty => _difficulty;
   
   public static Action OnAnswerSelected;
   public static Action<float, bool, int> OnAnswerConfirmed;
   public static Action<float, int, string> OnQuestionDataUpdate;
   
   public override void OnNetworkSpawn()
   {
      NetworkServicesHandler.OnQuestionUpdated += ReceiveQuestionValues;
      RoundUIController.OnIntroBannerClosed += DisplayQuestion;
      RoundManager.OnTimeOver += HideQuestion;
   }

   private void DisplayQuestion()
   {
      questionCanvas.gameObject.SetActive(true);
      questionHeader.gameObject.SetActive(true);
      questionHeader.transform.DOScale(Vector3.one, 0.1f);
      questionCanvas.transform.DOScale(Vector3.one, 0.1f);
   }

   private void HideQuestion()
   {
      answerCanvas.DOScale(Vector3.one, questionAnimationDuration).SetEase(Ease.InBack).OnComplete(() =>
      {
         answerCanvas.gameObject.SetActive(false);
         questionCanvas.transform.DOScale(Vector3.zero, questionAnimationDuration).SetEase(Ease.InBack).OnComplete(() =>
         {
            questionCanvas.gameObject.SetActive(false);
         });
      });
   }
   
   public void SetQuestion(string questionExpression, int questionDifficulty, bool usesExpression)
   {
      Queue<string> postFixQuestion = MathConverter.ConvertIntoPostfix(questionExpression,usesExpression, questionDifficulty,this);
      _answer = MathConverter.SolvePostfixExpression(postFixQuestion);
      _difficulty = questionDifficulty;
      GenerateAnswerButtons();

      BroadcastQuestionDataUpdateRpc(_answer, _difficulty, _infixExpression);
   }

   [Rpc(SendTo.ClientsAndHost)]
   private void BroadcastQuestionDataUpdateRpc(float answer, int difficulty, string infixExpression)
   {
      if(!IsOwner) return;
      OnQuestionDataUpdate?.Invoke(answer, difficulty, infixExpression);
   }
   
   private void ReceiveQuestionValues(float answer, int questionDifficulty, string questionExpression)
   {
      _infixExpression = questionExpression;
      _difficulty = questionDifficulty;
      _answer = answer;
      
      SetQuestionText();
   }
   
   public void SetInfixExpression(string newInfixExpression)
   {
      _infixExpression = newInfixExpression;
   }
   
   public void SelectAnswer(float answerSelected, AnswerButton button)
   {
      _selectedAnswer = answerSelected;

      if (_selectedAnswer == _answer)
      {
         _correctAnswer = true;
      }
      else
      {
         _correctAnswer = false;
      }

      if (Math.Abs(_selectedAnswer % 1) <= (Double.Epsilon * 100))
      {
         answerText.text = _selectedAnswer.ToString();

      }
      else
      {
         answerText.text = _selectedAnswer.ToString("F1");
      }
      
      _selectedButton = button;
      confirmButton.SetActive(true);
      OnAnswerSelected?.Invoke();
   }

   public void ConfirmAnswer()
   {
      SendAnswer(_selectedAnswer, _correctAnswer, _difficulty);
      
      confirmButton.SetActive(false);

      questionHeader.transform.localScale = Vector3.zero;
      questionHeader.gameObject.SetActive(false);

      answerCanvas.gameObject.SetActive(true);
      answerCanvas.DOScale(Vector3.one, questionAnimationDuration).SetEase(questionEase);
      
      foreach (var button in _answerButtonList)
      {
         button.gameObject.SetActive(false);
      }
      
      _selectedButton.SetButtonConfirmed();
   }

   
   private void SendAnswer(float answer, bool correctAnswer, int difficulty)
   {
      OnAnswerConfirmed?.Invoke(answer, correctAnswer, difficulty);
   }
   
   
   private void SetQuestionText()
   {
      questionTextComponent.text = _infixExpression;
   }

   private void GenerateAnswerButtons()
   {
      _alternativesList = new List<float>();
      
      if (IsServer)
      {
         for (int i = 0; i < ScriptableSettings.NUM_ALTERNATIVES-1; i++)
         {
            Vector2 minMaxRange = new Vector2(_answer * -2.5f, _answer * 2.5f);
            float newNumber = Random.Range(minMaxRange.x, minMaxRange.y);

            if (_alternativesList.Contains(newNumber) || newNumber == _answer || newNumber == 0)
            {
               newNumber = RegenerateAlternativeValue(newNumber);
            }
            
            if (Math.Abs(_answer % 1) <= (Double.Epsilon * 100))
            {
               _alternativesList.Add(Mathf.RoundToInt(newNumber));
            }
            else
            {
               _alternativesList.Add(newNumber);
            }
         }
         
         
         _alternativesList = GetFinalAlternativesList(ScriptableSettings.NUM_ALTERNATIVES);
         
         _alternativesList.Sort();
         
         if (_answerButtonList.IsNullOrEmpty())
         {
            for (int i = 0; i < _alternativesList.Count; i++)
            {
               GenerateButtonObjectsRpc(_alternativesList[i], i);
            }
         }
         else
         {
            for (int i = 0; i < _answerButtonList.Count; i++)
            {
               SetButtonObjectsRpc(i, _alternativesList[i]);
            }
         }
      }
   }
   
   [Rpc(SendTo.Everyone)]
   private void GenerateButtonObjectsRpc(float answer, int index)
   {
      if (_answerButtonList.IsNullOrEmpty())
      {
         _answerButtonList = new List<AnswerButton>();
      }
      
      GameObject answerButton = Instantiate(answerButtonPrefab, buttonsLayoutGroup);
      answerButton.name = $"ANSWER BUTTON {index}";
      AnswerButton answerButtonComponent = answerButton.GetComponent<AnswerButton>();

      answerButtonComponent.SetAnswerButtonValue(answer);
      _answerButtonList.Add(answerButtonComponent);
   }

   [Rpc(SendTo.Everyone)]
   private void SetButtonObjectsRpc(int index, float answer)
   {
      _answerButtonList[index].gameObject.SetActive(true);
      _answerButtonList[index].SetAnswerButtonValue(answer);
   }
   
   private float RegenerateAlternativeValue(float num1ToAvoid) //THIS IS MESSY AND NEEDS TO BE REWORKED.
   {
      float newNumber;
      
      do
      {
         Vector2 minMaxRange = new Vector2(_answer * -2.5f,
            _answer * 2.5f);
         newNumber = Random.Range(minMaxRange.x, minMaxRange.y);
      } while (newNumber == num1ToAvoid || newNumber == _answer || newNumber == 0);
      
      return newNumber;
   }
   
   private List<float> GetFinalAlternativesList(int count)
   {
      bool isRightAnswer = false;
      
      int currentAlternative = 0;

      List<float> finalList = new List<float>();

      for (int i = 0; i < count; i++)
      {
         if (!isRightAnswer)
         {
            var randomizeForRightAnswer = Random.Range(0,2);

            if (randomizeForRightAnswer == 1)
            {
               isRightAnswer = true;
               finalList.Add(_answer);
            }
            else
            {
               finalList.Add(_alternativesList[currentAlternative]); //THIS IS CAUSING ISSUES NOW AND THEN. NEED TO INVESTIGATE
               currentAlternative++;
            }
         }
         else
         {
            finalList.Add(_alternativesList[currentAlternative]);
            currentAlternative++;
         }

         if (currentAlternative > count - 1) break;
      }

      if (!isRightAnswer)
      {
         int randomButton = Random.Range(0, _answerButtonList.Count);

         finalList.Add(_answer);
      }

      return finalList;
   }

   public override void OnNetworkDespawn()
   {
      NetworkServicesHandler.OnQuestionUpdated -= ReceiveQuestionValues;
      RoundUIController.OnIntroBannerClosed -= DisplayQuestion;
      RoundManager.OnTimeOver -= HideQuestion;
      _moveButtonTween.Kill();
   }
}
